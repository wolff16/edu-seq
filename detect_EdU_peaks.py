#!/usr/bin/env python3
from collections import OrderedDict
from collections import defaultdict
import pyensembl
from functools import reduce
import subprocess
import argparse
from scipy.signal import find_peaks
import sys
import re
import copy
import pysam
import traceback
import pprint
import math
import pyBigWig
import time
import pandas as pd
from pyensembl import EnsemblRelease
from tqdm import tqdm
from functools import partial
import numpy as np
from os.path import exists
import csv

# script: detect_EdU_peaks.py
#  disclaimers: No claims are made that this script performs any specific task
#               not responsible for any damage/loss caused by using this script
#               script is to be used only for research purposes.
#
# Parameters we need
#
# * bin_size optional parameter will be defaulted to 10.000
#
# * quality , bam file quality parameter to filter on bam files, unique match
#
# * read length parameter important for perfect match -> maybe circumventable with a good python package to
#   also take variable length reads as a corner case as long as its a perfect match over its length
#
# * input bam file name
#
# * cell line adjustment file
##
# ./detect_EdU_peaks.py --verbose 1 prepare_reference -b ../bam/WT1_S1_R1_001_sorted.bam --read_length 130  --read_length_offset 50
# ./detect_EdU_peaks.py binning -b ../bam/Edu1_S2_R1_001_sorted.bam -a Reference_adjust_bin-size_10000_chr1-X_adjbin_0b.csv  -r 130 -o 50 -m 60 -d GRCh38 -t 15
# ./detect_EdU_peaks.py peak_calling -q ../bam/Edu1_S2_R1_001_sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_qual_counts_0b.txt -f ../bam/Edu1_S2_R1_001_sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_0b.csv -b ../bam/Edu1_S2_R1_001_sorted.bam
# ./detect_EdU_peaks.py  2bw -f ../../../bam/Edu1_S2_R1_001_sorted_subsampled_0.001.bam_sigma_all_0b_long_sorted.csv
# ./detect_EdU_peaks.py peak_annotation -f bam/Edu1_S2_R1_001_sorted.bam_sigma_all_0b.csv -c scripts/eduseq/edu-seq/data/Midas-seq-CFS-united_NEW_20231109.xlsx -r bam/Reference_adjust_bin-size_10000_chr1-X_adjbin_0b.csv -p 100


qual_OK = 0
qual_notOK = 0
####################
# helper functions #
####################


def longest_match(cigar_string):
    # Check if cigar_string is a valid string or bytes-like object
    if not isinstance(cigar_string, (str, bytes)):
        return 0

    # Extract all the 'M' operations
    matches = re.findall(r'(\d+)M', cigar_string)

    # Convert to integers and find the longest match
    return max(map(int, matches)) if matches else 0


def normal_round(n):
    if n - math.floor(n) < 0.5:
        return math.floor(n)
    return math.ceil(n)


def v_print(first, second, message, end="\n"):
    """
    Verbose print helper function:

    @param first: verbose parameter choosen by the user \n
    @param second: verbose level of print statement \n
    @param message: the print message to be printed (to standard error out)\n

    possible combinations:
        user choose 0:              no printing at all
                    1:              info level prints
                    2:              info and debug level prints

    """
    # the user choose to not see any print messages
    if first == 0:
        pass
    # The user choose to see the message but I commented it out
    elif second == 0:
        pass
    # This is info level of logging
    elif (first == 1) & second == 1:
        print(message, end=end, file=sys.stderr)
    elif (first == 2) & second == 1:
        print(message, end=end, file=sys.stderr)
    # This is debugging level of logging
    elif (first == 2) & (second == 2):
        print(message, end=end, file=sys.stderr)
    # verbose is info but we got a debug message-> dont print
    elif (first == 1) & (second == 2):
        pass
    # should not happen
    else:
        print(
            f"[CRITICAL] THIS log-message should not be possible!: {message}", end=end, file=sys.stderr)

# Print iterations progress


def printProgressBar(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', printEnd="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 *
                                                     (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    v_print(args.verbose, 1,
            f'\r{prefix} |{bar}| {percent}% {suffix}', end=printEnd)
    # Print New Line on Complete
    if iteration == total:
        v_print(args.verbose, 1, '', end=printEnd)


# use pysam to check header information with the hash-array of valid chromosome variations (done)
# do a small metric of how many chromosome could be correctly classified (done)
# GRCh38 (done)
# GenBank assembly accession:
#    GCA_000001405.29_GRCh38.p14_Genebank)
# RefSeq assembly accession:
#    GCF_000001405.40_GRCh38.p14_refseq)
# T2T-CHM13v2.0 (done)
# GenBank assembly accession:
#  GCA_009914755.4
# Refseq assembly accession:
#   GCF_009914755.1
# if a genome is selected correctly
# a subset with only these chromosome contigs should be applied and transfered to ucsc
# contig naming -> chr1,chr2 etc.
# for counting

# info based on: https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/001/405/GCF_000001405.40_GRCh38.p14/GCF_000001405.40_GRCh38.p14_assembly_report.txt
# and https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/009/914/755/GCF_009914755.1_T2T-CHM13v2.0/GCF_009914755.1_T2T-CHM13v2.0_assembly_report.txt

# AssignedMolecule GenBankAccn RefSeqAccn SequenceLength UCSCstylename
# 1 CM000663.2 NC_000001.11 248956422 chr1
# 2 CM000664.2 NC_000002.12 242193529 chr2
# 3 CM000665.2 NC_000003.12 198295559 chr3
# 4 CM000666.2 NC_000004.12 190214555 chr4
# 5 CM000667.2 NC_000005.10 181538259 chr5
# 6 CM000668.2 NC_000006.12 170805979 chr6
# 7 CM000669.2 NC_000007.14 159345973 chr7
# 8 CM000670.2 NC_000008.11 145138636 chr8
# 9 CM000671.2 NC_000009.12 138394717 chr9
# 10 CM000672.2 NC_000010.11 133797422 chr10
# 11 CM000673.2 NC_000011.10 135086622 chr11
# 12 CM000674.2 NC_000012.12 133275309 chr12
# 13 CM000675.2 NC_000013.11 114364328 chr13
# 14 CM000676.2 NC_000014.9 107043718 chr14
# 15 CM000677.2 NC_000015.10 101991189 chr15
# 16 CM000678.2 NC_000016.10 90338345 chr16
# 17 CM000679.2 NC_000017.11 83257441 chr17
# 18 CM000680.2 NC_000018.10 80373285 chr18
# 19 CM000681.2 NC_000019.10 58617616 chr19
# 20 CM000682.2 NC_000020.11 64444167 chr20
# 21 CM000683.2 NC_000021.9 46709983 chr21
# 22 CM000684.2 NC_000022.11 50818468 chr22
# X CM000685.2 NC_000023.11 156040895 chrX
# Y CM000686.2 NC_000024.10 57227415 chrY
# MT J01415.2 NC_012920.1 16569 chrM


contigName2info = OrderedDict({
    "CM000663.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 248956422,
                   "UCSC-style": "chr1"
                   },
    "CM000664.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 242193529,
                   "UCSC-style": "chr2"
                   },
    "CM000665.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 198295559,
                   "UCSC-style": "chr3"
                   },
    "CM000666.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 190214555,
                   "UCSC-style": "chr4"
                   },
    "CM000667.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 181538259,
                   "UCSC-style": "chr5"
                   },
    "CM000668.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 170805979,
                   "UCSC-style": "chr6"
                   },
    "CM000669.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 159345973,
                   "UCSC-style": "chr7"
                   },
    "CM000670.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 145138636,
                   "UCSC-style": "chr8"
                   },
    "CM000671.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 138394717,
                   "UCSC-style": "chr9"
                   },
    "CM000672.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 133797422,
                   "UCSC-style": "chr10"
                   },
    "CM000673.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 135086622,
                   "UCSC-style": "chr11"
                   },
    "CM000674.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 133275309,
                   "UCSC-style": "chr12"
                   },
    "CM000675.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 114364328,
                   "UCSC-style": "chr13"
                   },
    "CM000676.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 107043718,
                   "UCSC-style": "chr14"
                   },
    "CM000677.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 101991189,
                   "UCSC-style": "chr15"
                   },
    "CM000678.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 90338345,
                   "UCSC-style": "chr16"
                   },
    "CM000679.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 83257441,
                   "UCSC-style": "chr17"
                   },
    "CM000680.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 80373285,
                   "UCSC-style": "chr18"
                   },
    "CM000681.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 58617616,
                   "UCSC-style": "chr19"
                   },
    "CM000682.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 64444167,
                   "UCSC-style": "chr20"
                   },
    "CM000683.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 46709983,
                   "UCSC-style": "chr21"
                   },
    "CM000684.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 50818468,
                   "UCSC-style": "chr22"
                   },
    "CM000685.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 156040895,
                   "UCSC-style": "chrX"
                   },
    "CM000686.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                   "length": 57227415,
                   "UCSC-style": "chrY"
                   },
    "J01415.2": {"primary_assembly": "GCA_000001405.29_GRCh38.p14_Genebank",
                 "length": 16569,
                 "UCSC-style": "chrM"
                 },
    "NC_000001.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 248956422,
                     "UCSC-style": "chr1"
                     },
    "NC_000002.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 242193529,
                     "UCSC-style": "chr2"
                     },
    "NC_000003.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 198295559,
                     "UCSC-style": "chr3"
                     },
    "NC_000004.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 190214555,
                     "UCSC-style": "chr4"
                     },
    "NC_000005.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 181538259,
                     "UCSC-style": "chr5"
                     },
    "NC_000006.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 170805979,
                     "UCSC-style": "chr6"
                     },
    "NC_000007.14": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 159345973,
                     "UCSC-style": "chr7"
                     },
    "NC_000008.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 145138636,
                     "UCSC-style": "chr8"
                     },
    "NC_000009.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 138394717,
                     "UCSC-style": "chr9"
                     },
    "NC_000010.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 133797422,
                     "UCSC-style": "chr10"
                     },
    "NC_000011.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 135086622,
                     "UCSC-style": "chr11"
                     },
    "NC_000012.12": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 133275309,
                     "UCSC-style": "chr12"
                     },
    "NC_000013.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 114364328,
                     "UCSC-style": "chr13"
                     },
    "NC_000014.9": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                    "length": 107043718,
                    "UCSC-style": "chr14"
                    },
    "NC_000015.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 101991189,
                     "UCSC-style": "chr15"
                     },
    "NC_000016.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 90338345,
                     "UCSC-style": "chr16"
                     },
    "NC_000017.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 83257441,
                     "UCSC-style": "chr17"
                     },
    "NC_000018.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 80373285,
                     "UCSC-style": "chr18"
                     },
    "NC_000019.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 58617616,
                     "UCSC-style": "chr19"
                     },
    "NC_000020.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 64444167,
                     "UCSC-style": "chr20"
                     },
    "NC_000021.9": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                    "length": 46709983,
                    "UCSC-style": "chr21"
                    },
    "NC_000022.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 50818468,
                     "UCSC-style": "chr22"
                     },
    "NC_000023.11": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 156040895,
                     "UCSC-style": "chrX"
                     },
    "NC_000024.10": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                     "length": 57227415,
                     "UCSC-style": "chrY"
                     },
    "NC_012920.1": {"primary_assembly": "GCF_000001405.40_GRCh38.p14_refseq",
                    "length": 16569,
                    "UCSC-style": "chrM"
                    },
    "CP068277.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 248387328,
                   "UCSC-style": "chr1"
                   },
    "CP068276.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 242696752,
                   "UCSC-style": "chr2"
                   },
    "CP068275.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 201105948,
                   "UCSC-style": "chr3"
                   },
    "CP068274.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 193574945,
                   "UCSC-style": "chr4"
                   },
    "CP068273.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 182045439,
                   "UCSC-style": "chr5"
                   },
    "CP068272.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 172126628,
                   "UCSC-style": "chr6"
                   },
    "CP068271.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 160567428,
                   "UCSC-style": "chr7"
                   },
    "CP068270.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 146259331,
                   "UCSC-style": "chr8"
                   },
    "CP068269.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 150617247,
                   "UCSC-style": "chr9"
                   },
    "CP068268.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 134758134,
                   "UCSC-style": "chr10"
                   },
    "CP068267.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 135127769,
                   "UCSC-style": "chr11"
                   },
    "CP068266.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 133324548,
                   "UCSC-style": "chr12"
                   },
    "CP068265.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 113566686,
                   "UCSC-style": "chr13"
                   },
    "CP068264.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 101161492,
                   "UCSC-style": "chr14"
                   },
    "CP068263.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 99753195,
                   "UCSC-style": "chr15"
                   },
    "CP068262.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 96330374,
                   "UCSC-style": "chr16"
                   },
    "CP068261.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 84276897,
                   "UCSC-style": "chr17"
                   },
    "CP068260.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 80542538,
                   "UCSC-style": "chr18"
                   },
    "CP068259.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 61707364,
                   "UCSC-style": "chr19"
                   },
    "CP068258.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 66210255,
                   "UCSC-style": "chr20"
                   },
    "CP068257.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 45090682,
                   "UCSC-style": "chr21"
                   },
    "CP068256.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 51324926,
                   "UCSC-style": "chr22"
                   },
    "CP068255.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 154259566,
                   "UCSC-style": "chrX"
                   },
    "CP086569.2": {"primary_assembly": "GCA_009914755.4_T2T-CHM13v2.0_Genebank",
                   "length": 62460029,
                   "UCSC-style": "chrY"
                   },
    "CP068254.1": {"primary_assembly": "GCA_009914855.2_T2T-CHM13v2.0_Genebank",
                   "length": 16569,
                   "UCSC-style": "chrM"
                   },
    "NC_060925.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 248387328,
                    "UCSC-style": "chr1"
                    },
    "NC_060926.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 242696752,
                    "UCSC-style": "chr2"
                    },
    "NC_060927.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 201105948,
                    "UCSC-style": "chr3"
                    },
    "NC_060928.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 193574945,
                    "UCSC-style": "chr4"
                    },
    "NC_060929.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 182045439,
                    "UCSC-style": "chr5"
                    },
    "NC_060930.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 172126628,
                    "UCSC-style": "chr6"
                    },
    "NC_060931.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 160567428,
                    "UCSC-style": "chr7"
                    },
    "NC_060932.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 146259331,
                    "UCSC-style": "chr8"
                    },
    "NC_060933.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 150617247,
                    "UCSC-style": "chr9"
                    },
    "NC_060934.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 134758134,
                    "UCSC-style": "chr10"
                    },
    "NC_060935.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 135127769,
                    "UCSC-style": "chr11"
                    },
    "NC_060936.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 133324548,
                    "UCSC-style": "chr12"
                    },
    "NC_060937.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 113566686,
                    "UCSC-style": "chr13"
                    },
    "NC_060938.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 101161492,
                    "UCSC-style": "chr14"
                    },
    "NC_060939.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 99753195,
                    "UCSC-style": "chr15"
                    },
    "NC_060940.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 96330374,
                    "UCSC-style": "chr16"
                    },
    "NC_060941.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 84276897,
                    "UCSC-style": "chr17"
                    },
    "NC_060942.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 80542538,
                    "UCSC-style": "chr18"
                    },
    "NC_060943.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 61707364,
                    "UCSC-style": "chr19"
                    },
    "NC_060944.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 66210255,
                    "UCSC-style": "chr20"
                    },
    "NC_060945.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 45090682,
                    "UCSC-style": "chr21"
                    },
    "NC_060946.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 51324926,
                    "UCSC-style": "chr22"
                    },
    "NC_060947.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 154259566,
                    "UCSC-style": "chrX"
                    },
    "NC_060948.1": {"primary_assembly": "GCF_009914755.1_T2T-CHM13v2.0_refseq",
                    "length": 62460029,
                    "UCSC-style": "chrY"
                    },
    "na": {"primary_assembly": "na_T2T-CHM13v2.0_refseq",
           "length": 16569,
           "UCSC-style": "chrM"
           },
    "chr1": {"primary_assembly": "GRCh38",
             "length": 248956422,
             "UCSC-style": "chr1"
             },
    "chr2": {"primary_assembly": "GRCh38",
             "length": 242193529,
             "UCSC-style": "chr2"
             },
    "chr3": {"primary_assembly": "GRCh38",
             "length": 198295559,
             "UCSC-style": "chr3"
             },
    "chr4": {"primary_assembly": "GRCh38",
             "length": 190214555,
             "UCSC-style": "chr4"
             },
    "chr5": {"primary_assembly": "GRCh38",
             "length": 181538259,
             "UCSC-style": "chr5"
             },
    "chr6": {"primary_assembly": "GRCh38",
             "length": 170805979,
             "UCSC-style": "chr6"
             },
    "chr7": {"primary_assembly": "GRCh38",
             "length": 159345973,
             "UCSC-style": "chr7"
             },
    "chr8": {"primary_assembly": "GRCh38",
             "length": 145138636,
             "UCSC-style": "chr8"
             },
    "chr9": {"primary_assembly": "GRCh38",
             "length": 138394717,
             "UCSC-style": "chr9"
             },
    "chr10": {"primary_assembly": "GRCh38",
              "length": 133797422,
              "UCSC-style": "chr10"
              },
    "chr11": {"primary_assembly": "GRCh38",
              "length": 135086622,
              "UCSC-style": "chr11"
              },
    "chr12": {"primary_assembly": "GRCh38",
              "length": 133275309,
              "UCSC-style": "chr12"
              },
    "chr13": {"primary_assembly": "GRCh38",
              "length": 114364328,
              "UCSC-style": "chr13"
              },
    "chr14": {"primary_assembly": "GRCh38",
              "length": 107043718,
              "UCSC-style": "chr14"
              },
    "chr15": {"primary_assembly": "GRCh38",
              "length": 101991189,
              "UCSC-style": "chr15"
              },
    "chr16": {"primary_assembly": "GRCh38",
              "length": 90338345,
              "UCSC-style": "chr16"
              },
    "chr17": {"primary_assembly": "GRCh38",
              "length": 83257441,
              "UCSC-style": "chr17"
              },
    "chr18": {"primary_assembly": "GRCh38",
              "length": 80373285,
              "UCSC-style": "chr18"
              },
    "chr19": {"primary_assembly": "UCSC-style",
              "length": 58617616,
              "UCSC-style": "chr19"
              },
    "chr20": {"primary_assembly": "GRCh38",
              "length": 64444167,
              "UCSC-style": "chr20"
              },
    "chr21": {"primary_assembly": "GRCh38",
              "length": 46709983,
              "UCSC-style": "chr21"
              },
    "chr22": {"primary_assembly": "GRCh38",
              "length": 50818468,
              "UCSC-style": "chr22"
              },
    "chrX": {"primary_assembly": "GRCh38",
             "length": 156040895,
             "UCSC-style": "chrX"
             },
    "chrY": {"primary_assembly": "GRCh38",
             "length": 57227415,
             "UCSC-style": "chrY"
             },
    "chrM": {"primary_assembly": "GRCh38",
             "length": 16569,
             "UCSC-style": "chrM"
             }, })
# T2T genome contigs for the case of T2T USCS chromosome numbers: chr1...
T2TcontigName2info = OrderedDict({"chr1": {"primary_assembly": "T2T-CHM13",
                                           "length": 248387328,
                                           "UCSC-style": "chr1"
                                           },
                                  "chr2": {"primary_assembly": "T2T-CHM13",
                                           "length": 242696752,
                                           "UCSC-style": "chr2"
                                           },
                                  "chr3": {"primary_assembly": "T2T-CHM13",
                                           "length": 201105948,
                                           "UCSC-style": "chr3"
                                           },
                                  "chr4": {"primary_assembly": "T2T-CHM13",
                                           "length": 193574945,
                                           "UCSC-style": "chr4"
                                           },
                                  "chr5": {"primary_assembly": "T2T-CHM13",
                                           "length": 182045439,
                                           "UCSC-style": "chr5"
                                           },
                                  "chr6": {"primary_assembly": "T2T-CHM13",
                                           "length": 172126628,
                                           "UCSC-style": "chr6"
                                           },
                                  "chr7": {"primary_assembly": "T2T-CHM13",
                                           "length": 160567428,
                                           "UCSC-style": "chr7"
                                           },
                                  "chr8": {"primary_assembly": "T2T-CHM13",
                                           "length": 146259331,
                                           "UCSC-style": "chr8"
                                           },
                                  "chr9": {"primary_assembly": "T2T-CHM13",
                                           "length": 150617247,
                                           "UCSC-style": "chr9"
                                           },
                                  "chr10": {"primary_assembly": "T2T-CHM13",
                                            "length": 134758134,
                                            "UCSC-style": "chr10"
                                            },
                                  "chr11": {"primary_assembly": "T2T-CHM13",
                                            "length": 135127769,
                                            "UCSC-style": "chr11"
                                            },
                                  "chr12": {"primary_assembly": "T2T-CHM13",
                                            "length": 133324548,
                                            "UCSC-style": "chr12"
                                            },
                                  "chr13": {"primary_assembly": "T2T-CHM13",
                                            "length": 113566686,
                                            "UCSC-style": "chr13"
                                            },
                                  "chr14": {"primary_assembly": "T2T-CHM13",
                                            "length": 101161492,
                                            "UCSC-style": "chr14"
                                            },
                                  "chr15": {"primary_assembly": "T2T-CHM13",
                                            "length": 99753195,
                                            "UCSC-style": "chr15"
                                            },
                                  "chr16": {"primary_assembly": "T2T-CHM13",
                                            "length": 96330374,
                                            "UCSC-style": "chr16"
                                            },
                                  "chr17": {"primary_assembly": "T2T-CHM13",
                                            "length": 84276897,
                                            "UCSC-style": "chr17"
                                            },
                                  "chr18": {"primary_assembly": "T2T-CHM13",
                                            "length": 80542538,
                                            "UCSC-style": "chr18"
                                            },
                                  "chr19": {"primary_assembly": "T2T-CHM13",
                                            "length": 61707364,
                                            "UCSC-style": "chr19"
                                            },
                                  "chr20": {"primary_assembly": "T2T-CHM13",
                                            "length": 66210255,
                                            "UCSC-style": "chr20"
                                            },
                                  "chr21": {"primary_assembly": "T2T-CHM13",
                                            "length": 45090682,
                                            "UCSC-style": "chr21"
                                            },
                                  "chr22": {"primary_assembly": "T2T-CHM13",
                                            "length": 51324926,
                                            "UCSC-style": "chr22"
                                            },
                                  "chrX": {"primary_assembly": "T2T-CHM13",
                                           "length": 154259566,
                                           "UCSC-style": "chrX"
                                           },
                                  "chrY": {"primary_assembly": "T2T-CHM13",
                                           "length": 62460029,
                                           "UCSC-style": "chrY"
                                           },
                                  })


def check_genome_version(index_statistics_list, default):
    # check if ucsc contig ids are used by default,
    # if so we cant judge which genome we got here
    # for this we need a genome default parameter
    # this is important as the GRCh38 and T2T genome rather different

    # using a nested dictionary for fast lookups of contig info
    # contigName2info["NC_000001.11"]["primary_assembly"]
    # 'GCF_000001405.40_GRCh38.p14_refseq'
    # >>> contigName2info["NC_000001.11"]["length"]
    # 248956422
    # >>> contigName2info["NC_000001.11"]["UCSC-style"]
    # 'chr1'
    # >>> contigName2info["NC_000001.11"]
    # {'primary_assembly': 'GCF_000001405.40_GRCh38.p14_refseq', 'length': 248956422, 'UCSC-style': 'chr1'}

    genome_dict = OrderedDict({
        "GRCh38": 0,
        "T2T-CHM13": 0,
        "GCA_009914755.4_T2T-CHM13v2.0_Genebank": 0,
        "GCF_000001405.40_GRCh38.p14_refseq": 0,
        "GCA_000001405.29_GRCh38.p14_Genebank": 0,
        "GCF_009914755.1_T2T-CHM13v2.0_refseq": 0,
    })
    genome_save = OrderedDict({})
    FLAG = "NA"
    for item in index_statistics_list:
        # lookup up the contig name and if not present catch error and do nothing
        # else count up
        # item[0] is the name : contig='NW_003315945.1', mapped=155, unmapped=0, total=155
        try:
            if default == "T2T-CHM13":
                subset_contig = T2TcontigName2info[item[0]]
            elif default == "GRCh38":
                subset_contig = contigName2info[item[0]]
            else:
                v_print(
                    args.verbose, 1, f"The defined genome version is not known ({default}), exiting...")
                sys.exit()
            v_print(args.verbose, 2, f"Subsetconfig {subset_contig}")
            v_print(args.verbose, 2, f"Subset item {item}")
            # add the selected genome contig and save additional info for potential later use
            genome_save[item[0]] = OrderedDict({"contig": item[0],
                                                "UCSC-style": subset_contig["UCSC-style"],
                                                "primary_assembly": subset_contig["primary_assembly"],
                                                "length": subset_contig["length"],
                                                "mapped": item[1],
                                                "unmapped": item[2],
                                                "total": item[3]
                                                })
            # GCA_009914755.4_T2T-CHM13v2.0_Genebank
            # GCF_000001405.40_GRCh38.p14_refseq
            # GCA_000001405.29_GRCh38.p14_Genebank
            # GCF_009914755.1_T2T-CHM13v2.0_refseq
            # ucsc check ? -> chr1 etc
            if subset_contig["primary_assembly"] == default:
                genome_dict[default] += 1
            if subset_contig["primary_assembly"] == "GCA_009914755.4_T2T-CHM13v2.0_Genebank":
                genome_dict["GCA_009914755.4_T2T-CHM13v2.0_Genebank"] += 1
            elif subset_contig["primary_assembly"] == "GCF_000001405.40_GRCh38.p14_refseq":
                genome_dict["GCF_000001405.40_GRCh38.p14_refseq"] += 1
            elif subset_contig["primary_assembly"] == "GCA_000001405.29_GRCh38.p14_Genebank":
                genome_dict["GCA_000001405.29_GRCh38.p14_Genebank"] += 1
            elif subset_contig["primary_assembly"] == "GCF_009914755.1_T2T-CHM13v2.0_refseq":
                genome_dict["GCF_009914755.1_T2T-CHM13v2.0_refseq"] += 1
        except KeyError as error:
            v_print(args.verbose, 2,
                    f"[WARNING]: {repr(error)}, not found!")

    v_print(args.verbose, 2,
            f'ucsc: {genome_dict[default]};GRCh38_genebank: {genome_dict["GCA_000001405.29_GRCh38.p14_Genebank"]};GRCh38_refseq_counter: {genome_dict["GCF_000001405.40_GRCh38.p14_refseq"]}')
    v_print(args.verbose, 2,
            f'CHM13v2_genebank: {genome_dict["GCA_009914755.4_T2T-CHM13v2.0_Genebank"]};CHM13v2_refseq_counter: {genome_dict["GCF_000001405.40_GRCh38.p14_refseq"]}')
    FLAG = max(genome_dict, key=lambda k: genome_dict[k])
    v_print(args.verbose, 1,
            f'The identified genome is {FLAG} with {genome_dict[FLAG]} found chromosomes. ')
    v_print(args.verbose, 1, f"Printing saved chromosome information:")
    for item in genome_save:
        v_print(args.verbose, 1, f"{genome_save[item]}")
        pass

    # raise a warning that we didnt find not 23 chromosomes
    if genome_dict[FLAG] < 23:
        v_print(args.verbose, 1,
                f'[WARNING]: number of matched chromosomes is lower than 23 ({genome_dict[FLAG]})!\n')

    # raise a INFO that the UCSC naming is used and that T2T is assumed to be the genome
    if FLAG == "UCSC-style" and default == "T2T-CHM13":
        v_print(args.verbose, 1,
                f'[INFO]: The genome contig have the UCSC-style (chr1,...) and genome_default is set to {default} therefore genome annotation for {default} is used.\n')

    return (genome_save, FLAG)

# TODO
# It is possible to implement all 3 perl script into one python script
# with subparsers: example git checkout; git commit; git add etc...

# * SETUP OPTION PARSER                                                                     (done for 1/3 scripts)
# ** add subparsers if the scripts grows/ implements the other perl scripts                 (done for 1/3 scripts)


def apply_blacklisting(bin_matrix, adjust_bin_matrix, min, max):
    """
    Apply a blacklisting process to the provided matrices.

    This function iterates through the `adjust_bin_matrix` and sets values 
    outside the specified range (`min` and `max`) to zero. Corresponding values 
    in the `bin_matrix` are also set to zero.

    Parameters:
    - bin_matrix (pd.DataFrame): The main bin matrix.
    - adjust_bin_matrix (pd.DataFrame): The matrix with adjusted values to be checked against the range.
    - min (int/float): The minimum threshold for the blacklisting process.
    - max (int/float): The maximum threshold for the blacklisting process.

    Returns:
    - tuple: A tuple containing the modified `bin_matrix` and `adjust_bin_matrix`.
    """
    # Replace NaN values with 0 for both matrices
    local_bin_matrix = bin_matrix.fillna(0)
    local_adjust_bin_matrix = adjust_bin_matrix.fillna(0)

    # Iterate through each cell in the adjust_bin_matrix
    for row in range(local_adjust_bin_matrix.shape[0]-1):
        for col in range(local_adjust_bin_matrix.shape[1]-1):

            # Check if the value is within the threshold or already zero
            if ((min <= local_adjust_bin_matrix.iat[row, col] <= max) or local_adjust_bin_matrix.iat[row, col] == 0):
                # Value is within the threshold or already zero, so do nothing
                pass
            else:
                # Value is outside the threshold, so set it to zero
                v_print(
                    args.verbose, 2, f'Found a bin to be set to zero: row:{row},col:{col}|value:{local_adjust_bin_matrix.iloc[[row],[col]]}')
                local_adjust_bin_matrix.iloc[[row], [col]] = 0

                # Set the corresponding value in the bin_matrix to zero
                # Note: The bin_matrix has an offset of 3 columns before the chromosome data starts
                local_bin_matrix.iloc[[row], [col+3]] = 0

    return local_bin_matrix, local_adjust_bin_matrix


parser = argparse.ArgumentParser(
    description='Peak detection for Edu-Seq: binning, peak calling, annotation and conversion to .bw for genome browser visualisations.')
parser.add_argument('-v', '--verbose', metavar='v', type=int,
                    default=1, help='levels: 0-none,1-info,2-debug')
subparser = parser.add_subparsers(dest="scripts")
prepare_reference = subparser.add_parser('prepare_reference')
prepare_reference.add_argument('-b', '--bam', type=str,
                               required=True, help="Aligned bam file from the refence. The resulting count file will serve as the adjustment WGS sample file")
prepare_reference.add_argument('-s', '--bin_size', type=int, default=10000,
                               help="The size of each bin for the genome. (default: %(default)s)")
prepare_reference.add_argument('-r', '--read_length', type=int, default=100,
                               help="The size of each read. (default: %(default)s)")
prepare_reference.add_argument('-o', '--read_length_offset', type=int, default=5,
                               help="If you want to also have shorter reads as part of high quality filtering. (default: %(default)s)")
# We may can make the read length argument obsolete as its only there to check a perfect aligned read: like 100M
# but if reads were trimmed, a read lesser nucleotides but still perfect match was not catched in the old perl script
prepare_reference.add_argument('-m', '--mapping_quality', type=int, default=60,
                               help="Quality threshold of mapped reads to choose for the reference adjust file. (default score from bwa-mem: %(default)s)")
# unique /best mapping quality 42 -bowtie2; bwa= 37;bwa-mem default = 60
prepare_reference.add_argument('-d', '--default_ucsc', type=str, default="GRCh38",
                               help="Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: %(default)s")
prepare_reference.add_argument('-t', '--threads', type=int, default=1,
                               help="Number of threads to use for bam file processing: %(default)s")

binning = subparser.add_parser('binning')
binning.add_argument('-b', '--bam', type=str,
                     required=True, help="Aligned bam file.")
binning.add_argument('-s', '--bin_size', type=int, default=10000,
                     help="The size of each bin for the genome. (default: %(default)s)")
binning.add_argument('-a', '--adjust', type=str, required=True, default="",
                     help="Countmatrix file of the adjustment WGS sample: %(default)s")
binning.add_argument('-r', '--read_length', type=int, default=100,
                     help="The size of each read. (default: %(default)s)")
binning.add_argument('-o', '--read_length_offset', type=int, default=5,
                     help="If you want to also have shorter reads as part of high quality filtering. (default: %(default)s)")
# We may can make the read length argument obsolete as its only there to check a perfect aligned read: like 100M
# but if reads were trimmed, a read lesser nucleotides but still perfect match was not catched in the old perl script
binning.add_argument('-m', '--mapping_quality', type=int, default=60,
                     help="Quality threshold of mapped reads to choose for binning. (default score from bwa-mem: %(default)s)")
# unique /best mapping quality 42 -bowtie2; bwa= 37;bwa-mem default = 60
binning.add_argument('-d', '--default_ucsc', type=str, default="GRCh38",
                     help="Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: %(default)s")
binning.add_argument('-t', '--threads', type=int, default=1,
                     help="Number of threads to use for bam file processing: %(default)s")

peak_calling = subparser.add_parser('peak_calling')

peak_calling.add_argument('-q', '--quality_file', type=str,
                          required=True, help="Quality statistics file from binning.")
peak_calling.add_argument('-f', '--binning_file', type=str,
                          required=True, help="Binning text-file.")
peak_calling.add_argument('-b', '--bam', type=str,
                          required=True, help=".bam binary alignment file.")
peak_calling.add_argument('-t', '--threads', type=int, default=1,
                          help="Number of threads to use for bam file processing: %(default)s")
peak_calling.add_argument('-d', '--default_ucsc', type=str, default="GRCh38",
                          help="Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: %(default)s")


bigwig_conversion = subparser.add_parser('2bw', description="""
 INPUTFILE: -------_sigma_all_0b_long_sorted.csv
#
# for each genomic bin in the _sigma_all_0b.csv : 0 -> 0-9999, 1-19999

# different output files are added now as bigwig files: [...].bw


 14 = bin                            number of sequences per bin
  5 = adjust                         sequence coverage of genomic DNA sequencing (reflecting all sequencing biases)
 11 = adjbin                         number of sequences per bin, adjusted for all sequencing biases (using $adjust)
 12 = adjbin_mb                      number of sequences per bin with background (noise) subtraction, adjusted for all sequencing biases
 
  7 = sigma_bin_totalSD              sigma value without background (noise) subtraction
  3 = sigma_bin_totalSD_sm           sigma value without background (noise) subtraction, all values smoothened
  4 = sigma_bin_totalSD_tm           sigma value without background (noise) subtraction, high values trimmed

  6 = $sigma_bin_mb_totalSD           sigma value with background (noise) subtraction
  1 = $sigma_bin_mb_totalSD_sm        sigma value with background (noise) subtraction, all values smoothened
  2 = $sigma_bin_mb_totalSD_tm        sigma value with background (noise) subtraction, high values trimmed

  8 = $sigma_bin_mb_totalSD_log2      sigma value with background (noise) subtraction, converted to log2
  9 = $sigma_bin_mb_totalSD_sm_log2   sigma value with background (noise) subtraction, all values smoothened, converted to log2
 10 = $sigma_bin_mb_totalSD_tm_log2   sigma value with background (noise) subtraction, high values trimmed, converted to log2

 13 = adjbin_SD                       standard deviation of adjbin values

                                         """, formatter_class=argparse.RawDescriptionHelpFormatter)
bigwig_conversion.add_argument('-f', '--csv_file', type=str,
                               required=True, help="The ...sigma_all_0b_long_sorted.csv file from --peak_calling option")
bigwig_conversion.add_argument('-s', '--bin_size', type=int,
                               required=False, default=10000, help="The used binsize.")
peak_annotation = subparser.add_parser('peak_annotation')
peak_annotation.add_argument('-f', '--csv_file', type=str,
                             required=True, help="Binning ...sigma_all_0b.csv text-file.")
peak_annotation.add_argument('-r', '--ref_file', type=str,
                             required=True, help="Reference text file ...sigma_all_0b.csv text-file.")
peak_annotation.add_argument('-s', '--bin_size', type=int,
                             required=False, default=10000, help="The used binsize. default: %(default)s")
peak_annotation.add_argument('-t', '--threads', type=int, default=10,
                             help="Number if threads to use for bam file processing: %(default)s")
peak_annotation.add_argument('-d', '--default_ucsc', type=str, default="GRCh38",
                             help="Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used (Annotation is based on GRCh38 for now): %(default)s")
peak_annotation.add_argument('-p', '--prominence_threshold', type=int, default=200,
                             help="scipy.singal.find_peaks parameter. The prominence of a peak measures how much a peak stands out from the surrounding baseline of the signal \n and is defined as the vertical distance between the peak and its lowest contour line: default %(default)s")
peak_annotation.add_argument('-c', '--fragile_sites', type=str, required=True,
                             help="Midas-seq-CFS-united20230710.xlsx, Fragile site excel file, part of the data folder")
peak_annotation.add_argument('-R', '--ratio', type=int,
                             required=False, default=4, help="The peak ratio between WT and Treatment peak to be shown in the bed file. This can be loaded into jbrowse as a track. ")


args = parser.parse_args()
# parser.print_help()
if args.scripts == 'prepare_reference':
    v_print(args.verbose, 1, 'Prepare reference script is running...')
    v_print(args.verbose, 0, f'Reference Object: {args.bam}')

    # try open files and work on them
    bam_exists = exists(args.bam)
    # does the file exist
    if (bam_exists):
        v_print(args.verbose, 1, f"Input file found: {args.bam}")
        sam_repl_file = args.bam
        bin_size = args.bin_size
        offset = args.read_length_offset
        # no need to overengineer if we wanna replicate the perl script closely
        chr_size = 250000000
        quality_score = args.mapping_quality
        number_bins = int(chr_size/bin_size)
        read_length = args.read_length
        # pylint: disable=maybe-no-member
        samfile = pysam.AlignmentFile(
            sam_repl_file, "rb", threads=args.threads)
        # check if its actually a bam file and got an index
        try:
            p = samfile.check_index()
            v_print(args.verbose, 1, f"checkindex: {p}")
            sam_list = samfile.get_index_statistics()
            v_print(args.verbose, 1, f"checkindex: {sam_list}")

            selected_genome_info, FLAG = check_genome_version(
                sam_list, default=args.default_ucsc)

            # read bin hits
            #
            #

            #
            # create array for bin hits
            #

            # python init so we get not used values via '' mimicing 'undef' of perl
            bin_hits = pd.DataFrame("", columns=range(
                0, number_bins+1), index=range(0, 23+1))
            # perl way , 0 will be 'undefined'
            for loopc in range(1, 23+1):
                for loopb in range(0, number_bins+1):
                    bin_hits.iat[loopc, loopb] = 0

            # readlength = f"{args.read_length}M"
            i = 0
            qualOK = 0
            qualnotOK = 0
            chr_size = list()
            chr_name_l = list()
            # normal contig listing
            if FLAG != "GCA_000001405.29_GRCh38.p14_Genebank":
                for chr in range(0, len(selected_genome_info.items())):
                    chr_name = list(list(selected_genome_info.items())[
                                    chr][1].items())[1][1]
                    chr_length = list(list(selected_genome_info.items())[
                                      chr][1].items())[3][1]
                    v_print(args.verbose, 2,
                            f"name: {chr_name} length: {chr_length}")
                    chr_size.append(chr_length/int(bin_size))
                    chr_name_l.append(chr_name)
                    v_print(args.verbose, 2, f"{chr_size[chr]}")
            # gene bank contig listing
            else:
                for chr in range(0, len(selected_genome_info.items())):
                    chr_name = list(list(selected_genome_info.items())[
                                    chr][1].items())[0][1]
                    chr_length = list(list(selected_genome_info.items())[
                                      chr][1].items())[3][1]
                    v_print(args.verbose, 2,
                            f"name: {chr_name} length: {chr_length}")
                    chr_size.append(chr_length/int(bin_size))
                    chr_name_l.append(chr_name)
                    v_print(args.verbose, 2, f"{chr_size[chr]}")

            v_print(args.verbose, 1, chr_name_l)

            printProgressBar(0, samfile.mapped,
                             prefix='filtering for Quality reads:', suffix='Complete', length=50)
            for read in samfile:
                v_print(args.verbose, 0, f"{read.cigarstring}")
                i = i+1
                # get_blocks() is the zero based start of the read,
                # the perl script used the original 1-based position directly from the samfile of the read
                # to judge into which bin it is to put, I will mirror it
                # therefore we add 1 to it

                # chrM gets ignored in the first hand and no exceptional chrM should be in here but we will negate it to be sure
                # check if the chromosome is there
                count = chr_name_l.count(read.reference_name)
                v_print(args.verbose, 0,
                        f"{chr_name_l}  {read.reference_name} ,{FLAG}")

                if count > 0:
                    chr_number = chr_name_l.index(read.reference_name)+1
                else:
                    chr_number = 26
                # no chrY and chrM
                v_print(
                    args.verbose, 2, f"{read.cigarstring} {read.mapping_quality} {read.reference_name} {count} {chr_number}   ")
                if longest_match(read.cigarstring) >= read_length - offset and read.mapping_quality >= quality_score and read.reference_name != "chrM" and count > 0 and chr_number < 24:
                    # the orginal implementation only used == readlength but high quality reads with a shorter readlength should be also considered is my stand
                    # the >= in quality_score is more of a future_proof, if they decide to increase mapping quality scores to > 60
                    pos = read.get_blocks()[0][0]+1
                    # again zero based -> +1, but bin_hits is starting with index 1, index 0 is undef
                    chr_number = chr_name_l.index(read.reference_name)+1
                    # print(f"{chr_number},{read.reference_name}")
                    qualOK += 1
                    # check the read pos and divide it by binsize -> gets the 'bucket' on where to count +1
                    bin_allocate = pos/bin_size
                    bin_allocate_int = int(bin_allocate)  # make it integer
                    # print(f"value: {chr_number} :{bin_allocate_int}")
                    # print(f"value: {bin_hits.iat[chr_number,bin_allocate_int]}")
                    bin_hits.iat[chr_number, bin_allocate_int] += 1
                else:
                    qualnotOK += 1

                if i % 1000000 == 0:
                    printProgressBar(i, samfile.mapped,
                                     prefix='filtering for Quality reads:', suffix='Complete', length=50)
                if i % 5000000 == 0:
                    v_print(args.verbose, 2, f"quality OK {qualOK}")
                    v_print(args.verbose, 2, f"quality not OK {qualnotOK}\n")

            # eliminate bin and adjust hits outside low and high exclude range
            v_print(args.verbose, 1, f"\nquality OK {qualOK}")
            v_print(args.verbose, 1, f"quality not OK {qualnotOK}")
            # no blacklisting for reference !!!
            #     # exclude values for adjust hit counts - blacklisting//masking
            #
            # Write Files
            # adjbin_hits.to_csv("adjbin_hits.csv")
            result_file = f"Reference_adjust_bin-size_{bin_size}_chr1-X_adjbin_0b.csv"

            with open(result_file, 'w') as RESULT:
                for loopb in range(0, number_bins+1):
                    for loopc in range(1, 23+1):
                        if loopc == 23:
                            RESULT.write(f"{bin_hits.iat[loopc,loopb]}")
                        else:
                            RESULT.write(f"{bin_hits.iat[loopc,loopb]},")
                    RESULT.write("\n")
        except AttributeError:
            # v_print(args.verbose,1,
            #    f"[ERROR] The Alignment-file is SAM formatted and thus has no index: {args.bam}.")
            v_print(args.verbose, 1, f"{traceback.print_exc()}")
            samfile.close()
        # except ValueError as verror:
        #    print(
        #        f"[Value-Error] {verror}.")
        #    samfile.close()
    else:
        v_print(args.verbose, 1,
                f"[ERROR] Input bam file not found: {args.bam}, script stopped.")
        SystemExit(1)
    pass
elif args.scripts == 'binning':
    v_print(args.verbose, 1, 'Binning script is running...')
    v_print(args.verbose, 0, f'Binning Object: {args.bam}')

    ####  DO BINNING STUFF HERE  ####

    # try open files and work on them
    bam_exists = exists(args.bam)
    # does the file exist
    if (bam_exists):

        v_print(args.verbose, 1, f"Input file found: {args.bam}")
        sam_repl_file = args.bam
        bin_size = args.bin_size
        offset = args.read_length_offset
        # no need to overengineer if we wanna replicate the perl script closely
        chr_size = 250000000
        quality_score = args.mapping_quality
        number_bins = int(chr_size/bin_size)
        read_length = args.read_length
        # pylint: disable=maybe-no-member
        samfile = pysam.AlignmentFile(
            sam_repl_file, "rb", threads=args.threads)
        # check if its actually a bam file and got an index
        try:
            p = samfile.check_index()
            v_print(args.verbose, 1, f"checkindex: {p}")
            sam_list = samfile.get_index_statistics()
            v_print(args.verbose, 1, f"checkindex: {sam_list}")

            selected_genome_info, FLAG = check_genome_version(
                sam_list, default=args.default_ucsc)

            # READ IN DATA

            # adjust_bin_matrix=pd.read_csv("U2OS_adjust__bin-size_10000_chr1-X_adjbin_0b.csv",header=None) # this would be the normal way,
            #  we will write it the way it was implemented in the perl script
            loopb = 0  # zero base
            number_OK_adjust_lines = 0
            number_notOK_adjust_lines = 0

            adjust_hits = pd.DataFrame("", columns=range(
                0, number_bins+1), index=range(0, 23+1))
            with open(args.adjust) as f:
                for line in f:
                    array_line = line.split(',')  # split each line
                    # print(len(array_line))
                    # print(array_line)

                    if (len(array_line) == 23):
                        for loopc in range(1, 23+1):
                            adjust_hits.iat[loopc, loopb] = array_line[loopc-1]

                        loopb += 1
                        number_OK_adjust_lines += 1
                    else:
                        number_notOK_adjust_lines += 1

            # print(bin_matrix)

            v_print(args.verbose, 1, f"\nBin size:      {bin_size}")
            v_print(args.verbose, 1, f"Quality score: {quality_score}")
            v_print(args.verbose, 1, f"Read length:   {read_length}")
            #
            v_print(args.verbose, 1, f"Number of bins: {number_bins}")
            v_print(
                args.verbose, 1, f"Number of OK adjust lines (should be equal to number of bins or one more): {number_OK_adjust_lines}")
            v_print(
                args.verbose, 1, f"Number of not OK adjust lines (should be zero or one):                     {number_notOK_adjust_lines}")

            # read bin hits
            #
            #

            #
            # create array for bin hits
            #

            # python init so we get not used values via '' mimicing 'undef' of perl
            bin_hits = pd.DataFrame("", columns=range(
                0, number_bins+1), index=range(0, 23+1))
            # perl way , 0 will be 'undefined'
            for loopc in range(1, 23+1):
                for loopb in range(0, number_bins+1):
                    bin_hits.iat[loopc, loopb] = 0

            # readlength = f"{args.read_length}M"
            i = 0
            qualOK = 0
            qualnotOK = 0
            chr_size = list()
            chr_name_l = list()
            # normal contig listing
            if FLAG != "GCA_000001405.29_GRCh38.p14_Genebank":
                for chr in range(0, len(selected_genome_info.items())):
                    chr_name = list(list(selected_genome_info.items())[
                                    chr][1].items())[1][1]
                    chr_length = list(list(selected_genome_info.items())[
                                      chr][1].items())[3][1]
                    v_print(args.verbose, 2,
                            f"name: {chr_name} length: {chr_length}")
                    chr_size.append(chr_length/int(bin_size))
                    chr_name_l.append(chr_name)
                    v_print(args.verbose, 2, f"{chr_size[chr]}")
            # gene bank contig listing
            else:
                for chr in range(0, len(selected_genome_info.items())):
                    chr_name = list(list(selected_genome_info.items())[
                                    chr][1].items())[0][1]
                    chr_length = list(list(selected_genome_info.items())[
                                      chr][1].items())[3][1]
                    v_print(args.verbose, 2,
                            f"name: {chr_name} length: {chr_length}")
                    chr_size.append(chr_length/int(bin_size))
                    chr_name_l.append(chr_name)
                    v_print(args.verbose, 2, f"{chr_size[chr]}")

            v_print(args.verbose, 1, chr_name_l)

            printProgressBar(0, samfile.mapped,
                             prefix='filtering for Quality reads:', suffix='Complete', length=50)
            for read in samfile:
                v_print(args.verbose, 0, f"{read.cigarstring}")
                i = i+1
                # get_blocks() is the zero based start of the read,
                # the perl script used the original 1-based position directly from the samfile of the read
                # to judge into which bin it is to put, I will mirror it
                # therefore we add 1 to it

                # chrM gets ignored in the first hand and no exceptional chrM should be in here but we will negate it to be sure
                # check if the chromosome is there
                count = chr_name_l.count(read.reference_name)
                v_print(args.verbose, 0,
                        f"{chr_name_l}  {read.reference_name} ,{FLAG}")

                if count > 0:
                    chr_number = chr_name_l.index(read.reference_name)+1
                else:
                    chr_number = 26
                # no chrY and chrM
                v_print(
                    args.verbose, 0, f"{read.cigarstring} {read.mapping_quality} {read.reference_name} {count} {chr_number}   ")
                if longest_match(read.cigarstring) >= read_length-offset and read.mapping_quality >= quality_score and read.reference_name != "chrM" and count > 0 and chr_number < 24:
                    # the orginal implementation only used == readlength but high quality reads with a shorter readlength should be also considered is my stand
                    # the >= in quality_score is more of a future_proof, if they decide to increase mapping quality scores to > 60
                    pos = read.get_blocks()[0][0]+1
                    # again zero based -> +1, but bin_hits is starting with index 1, index 0 is undef
                    chr_number = chr_name_l.index(read.reference_name)+1
                    # print(f"{chr_number},{read.reference_name}")
                    qualOK += 1
                    # check the read pos and divide it by binsize -> gets the 'bucket' on where to count +1
                    bin_allocate = pos/bin_size
                    bin_allocate_int = int(bin_allocate)  # make it integer
                    # print(f"value: {chr_number} :{bin_allocate_int}")
                    # print(f"value: {bin_hits.iat[chr_number,bin_allocate_int]}")
                    bin_hits.iat[chr_number, bin_allocate_int] += 1
                else:
                    qualnotOK += 1

                if i % 1000000 == 0:
                    printProgressBar(i, samfile.mapped,
                                     prefix='filtering for Quality reads:', suffix='Complete', length=50)
                if i % 5000000 == 0:
                    v_print(args.verbose, 2, f"quality OK {qualOK}")
                    v_print(args.verbose, 2, f"quality not OK {qualnotOK}\n")

            # eliminate bin and adjust hits outside low and high exclude range
            v_print(args.verbose, 1, f"\nquality OK {qualOK}")
            v_print(args.verbose, 1, f"quality not OK {qualnotOK}")

            # exclude values for adjust hit counts - blacklisting//masking
            #
            if (bin_size == 10000):

                exclude_low = 25  # these values are good for bin_size of 10000
                exclude_high = 10000  # these values are good for bin_size of 10000
            elif (bin_size == 1000):
                exclude_low = 10  # these values are good for bin_size of 1000
                exclude_high = 2000  # these values are good for bin_size of 1000
            else:

                exclude_low = 1  # limit 0
                exclude_high = 1000000

            printProgressBar(0, 23*(number_bins+1),
                             prefix='blacklisting reads:', suffix='Complete', length=50)
            progress_i = 0
            for loopc in range(1, 23+1):
                for loopb in range(0, number_bins+1):
                    v_print(args.verbose, 2,
                            f"{type(adjust_hits.iat[loopc,loopb])}")
                    v_print(args.verbose, 2, f"{exclude_low}")
                    progress_i = progress_i+1
                    if (int(adjust_hits.iat[loopc, loopb]) < exclude_low or int(adjust_hits.iat[loopc, loopb]) > exclude_high):
                        bin_hits.iat[loopc, loopb] = 0
                        adjust_hits.iat[loopc, loopb] = 0
                    printProgressBar(progress_i, 23*(number_bins+1),
                                     prefix='blacklisting reads:', suffix='Complete', length=50)
            v_print(args.verbose, 1, f"\n")

            #  calculate total hits
            bin_total_hits = 0
            adjust_total_hits = 0
            printProgressBar(0, 23*(number_bins+1),
                             prefix='calc total hits:', suffix='Complete', length=50)
            progress_i = 0
            for loopc in range(1, 23+1):
                for loopb in range(0, number_bins+1):
                    progress_i = progress_i+1
                    bin_total_hits = bin_total_hits + \
                        int(bin_hits.iat[loopc, loopb])
                    adjust_total_hits = adjust_total_hits + \
                        int(adjust_hits.iat[loopc, loopb])
                    printProgressBar(progress_i, 23*(number_bins+1),
                                     prefix='calc total hits:', suffix='Complete', length=50)

            v_print(args.verbose, 1, f"\n")
            #
            #
            v_print(args.verbose, 1, f"Total sample hits: {bin_total_hits}")
            v_print(args.verbose, 1, f"Adjust total hits: {adjust_total_hits}")
            Acorr_factor = adjust_total_hits/bin_total_hits
            v_print(args.verbose, 1, f"Correction factor: {Acorr_factor}")
            #
            # adjust bin hits and calculate total adjbin hits
            adjbin_total_hits = 0
            adjbin_hits = pd.DataFrame("", columns=range(
                0, number_bins+1), index=range(0, 23+1))
            tmp = 0
            v_print(args.verbose, 1,
                    f"adjust bin hits and calculate total adjbin hits ")
            for loopc in range(1, 23+1):
                for loopb in range(0, number_bins+1):
                    if (int(adjust_hits.iat[loopc, loopb]) > 0):
                        adjbin_hits.iat[loopc, loopb] = int(
                            bin_hits.iat[loopc, loopb]*Acorr_factor*1000/int(adjust_hits.iat[loopc, loopb])+0.5)
                        adjbin_total_hits = adjbin_total_hits + \
                            adjbin_hits.iat[loopc, loopb]
                    else:
                        adjbin_hits.iat[loopc, loopb] = 0

            v_print(args.verbose, 1,
                    f"Total adjusted sample hits: {adjbin_total_hits}\n")
            #
            # Write Files
            # adjbin_hits.to_csv("adjbin_hits.csv")
            result_file = f"{sam_repl_file}_bin-size_{bin_size}_quality_{quality_score}_chr1-X_adjbin_qual_counts_0b.txt"

            with open(result_file, 'w') as RESULT:

                RESULT.write("\n")
                RESULT.write(f"file {sam_repl_file}\n")
                RESULT.write(f"quality OK {qualOK}\n")
                RESULT.write(f"quality not OK {qualnotOK}\n\n")
                RESULT.write(f"\n")
                RESULT.write(f"Total sample hits: {bin_total_hits}\n")
                RESULT.write(f"Adjust total hits: {adjust_total_hits}\n")
                RESULT.write(f"Correction factor: {Acorr_factor}\n")
                RESULT.write(
                    f"Total adjusted sample hits: {adjbin_total_hits}\n\n")

            result_file = f"{sam_repl_file}_bin-size_{bin_size}_quality_{quality_score}_chr1-X_adjbin_0b.csv"
            print(number_bins)

            with open(result_file, 'w') as RESULT:
                for loopb in range(0, number_bins+1):
                    line_data = []
                    for loopc in range(1, 23+1):
                        # it could happen that additionally newline sneak in
                        line_data.append(
                            f"{bin_hits.iat[loopc,loopb]}".replace('\n', '') +
                            f",{adjbin_hits.iat[loopc,loopb]}".replace('\n', '') +
                            f",{adjust_hits.iat[loopc,loopb]}".replace('\n', '') +
                            f","

                        )
                    RESULT.write(','.join(line_data) + "\n")
        except AttributeError:
            # v_print(args.verbose,1,
            #    f"[ERROR] The Alignment-file is SAM formatted and thus has no index: {args.bam}.")
            v_print(args.verbose, 1, f"{traceback.print_exc()}")
            samfile.close()
        # except ValueError as verror:
        #    print(
        #        f"[Value-Error] {verror}.")
        #    samfile.close()
    else:
        v_print(args.verbose, 1,
                f"[ERROR] Input bam file not found: {args.bam}, script stopped.")
        SystemExit(1)

elif args.scripts == 'peak_calling':

    v_print(args.verbose, 1, 'Sigma peak calling script is running...')
    # v_print(args.verbose, 0, f'Sigma Object: {args.bam}')

    # get the correction factor, total sample hits, total adjust hits

    # aw: general remarks in the perl script the loopC is for iterating over
    # chromosomes and loopA is for iterating over bins
    # chromosome looping should be columns according to the input csvs
    # but they come first in the perl script and  at least for
    # python these are row iterations:
    # these means the actual matrix is always 23x25000 (rows x columns)
    # instead of 25000x23 so i will swap it ab at the dataframe access
    # iat[loopA,loopC] -> iat[loopC,loopA]
    # aw: reversed back to the original perl script notation for easier comparison of indices and its underlying values

    quality_file_Name = args.quality_file
    file_Name = args.binning_file
    sam_repl_file = args.bam
    batch_cmd = f'grep "Correction factor" {quality_file_Name}'
    get_correction_factor = subprocess.check_output(batch_cmd, shell=True)
    v_print(args.verbose, 1, get_correction_factor)
    Acorr_factor = get_correction_factor.decode().split(':')[1]
    v_print(args.verbose, 1, Acorr_factor)

    batch_cmd = f'grep "Total sample hits" {quality_file_Name}'
    get_total_sample_hits = subprocess.check_output(batch_cmd, shell=True)
    v_print(args.verbose, 1, get_total_sample_hits)
    bin_total_hits = get_total_sample_hits.decode().split(':')[1]
    v_print(args.verbose, 1, bin_total_hits)
    v_print(args.verbose, 1, type(bin_total_hits))

    batch_cmd = f'grep "Adjust total hits" {quality_file_Name}'
    get_total_adjust_hits = subprocess.check_output(batch_cmd, shell=True)
    v_print(args.verbose, 1, get_total_adjust_hits)
    adjust_total_hits = get_total_adjust_hits.decode().split(':')[1]
    v_print(args.verbose, 1, adjust_total_hits)
    # get bin size
    #
    temp_bin_size_1 = file_Name.split("bin-size_")
    v_print(args.verbose, 1, temp_bin_size_1)
    temp_bin_size_2 = temp_bin_size_1[1].split("_")
    bin_size = int(temp_bin_size_2[0])
    v_print(args.verbose, 1, bin_size)
    max_chr_size = 250000000
    number_bins = int(max_chr_size/bin_size)
    max_number_bins = int(max_chr_size/int(bin_size))  # cast to integer
    v_print(args.verbose, 1, f"\nFile Name: {file_Name}")
    v_print(args.verbose, 1, f"Bin size: {bin_size}")
    v_print(args.verbose, 1, f"Max number of bins: {max_number_bins}")
    v_print(args.verbose, 1, f"Total sample hits: {bin_total_hits}")
    v_print(args.verbose, 1, f"Adjust Total hits: {adjust_total_hits}")
    v_print(args.verbose, 1,
            f"Correction factor from bin to adjbin: {Acorr_factor}\n\n")
    donothing = 0
    #
    # define variables; mb: minus mean baseline
    # this is the most ugly way to implement a data.frame but for direct implementation from perl
    # i will do it
    # concatenating rows as lists to dataframes is 100x faster
    # https://stackoverflow.com/questions/13784192/creating-an-empty-pandas-dataframe-then-filling-it
    #
    # initiation can be done in a one liner, the perl script part will be left there but commented

    bin = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))

    adjbin = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    adjbin_2 = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    adjbin_mb = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    adjbin_SD = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    #
    sigma_bin_totalSD = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_totalSD_sm = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_totalSD_tm = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_mb_totalSD = pd.DataFrame("", columns=range(
        0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_mb_totalSD_sm = pd.DataFrame(
        "", columns=range(0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_mb_totalSD_tm = pd.DataFrame(
        "", columns=range(0, max_number_bins+1), index=range(0, 23+1))
    #
    sigma_bin_mb_totalSD_log2 = pd.DataFrame(
        "", columns=range(0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_mb_totalSD_sm_log2 = pd.DataFrame(
        "", columns=range(0, max_number_bins+1), index=range(0, 23+1))
    sigma_bin_mb_totalSD_tm_log2 = pd.DataFrame(
        "", columns=range(0, max_number_bins+1), index=range(0, 23+1))
    #
    adjust = pd.DataFrame(0, columns=range(
        0, max_number_bins+1), index=range(0, 23+1))

    # perl script includes 0-25000 , range goes one less -> range(25000) -> 0-24999
    progress_i = 0
    printProgressBar(progress_i, max_number_bins*24,
                     prefix='init DataFrames... :', suffix='Complete', length=50)
    for loopA in range(0, max_number_bins+1):
        for loopC in range(1, 23+1):
            bin.iat[loopC, loopA] = 0                     # actual counts
            #
            # adjusted bin (adjbin)
            adjbin.iat[loopC, loopA] = 0
            # adjusted bin minus baseline mean
            adjbin_mb.iat[loopC, loopA] = 0
            adjbin_SD.iat[loopC, loopA] = 0                 # SD of adjbin
            #
            sigma_bin_totalSD.iat[loopC, loopA] = 0
            sigma_bin_totalSD_sm.iat[loopC, loopA] = 0
            sigma_bin_totalSD_tm.iat[loopC, loopA] = 0
            sigma_bin_mb_totalSD.iat[loopC, loopA] = 0
            sigma_bin_mb_totalSD_sm.iat[loopC, loopA] = 0
            sigma_bin_mb_totalSD_tm.iat[loopC, loopA] = 0

            sigma_bin_mb_totalSD_log2.iat[loopC, loopA] = 0
            sigma_bin_mb_totalSD_sm_log2.iat[loopC, loopA] = 0
            sigma_bin_mb_totalSD_tm_log2.iat[loopC, loopA] = 0

            adjust.iat[loopC, loopA] = 0
            progress_i = progress_i+1
            if progress_i % 100000 == 0:
                printProgressBar(progress_i, max_number_bins*24,
                                 prefix='init DataFrames... :', suffix='Complete', length=50)

    v_print(args.verbose, 1, bin.shape)
    v_print(args.verbose, 1, bin)
    # reads lines and puts them in variable line_Data  | aw: we can simply read this via read_csv
    #
    csv_Data = pd.read_csv(file_Name, header=None)
    v_print(args.verbose, 1, file_Name)
    v_print(args.verbose, 1, csv_Data)
    #
    # extract data
    #
    # perl script includes 0-25000 , range goes one less -> range(25000) -> 0-24999
    for loopA in range(0, max_number_bins+1):
        for loopC in range(1, 23+1):
            # aw: original was from 1-23 and loopC-1 | the csv hast 23 *3 +1 blank columns setup :aw changed back to original implementation
            handle = (loopC-1)*4
            # v_print(args.verbose, 1,handle)
            bin.iat[loopC, loopA] = csv_Data.iat[loopA,
                                                 handle]        # original counts
            # original counts divided by adjust and scaled
            adjbin.iat[loopC, loopA] = csv_Data.iat[loopA, handle+1]
            adjust.iat[loopC, loopA] = csv_Data.iat[loopA,
                                                    handle+2]   # adjust counts

    v_print(args.verbose, 1, bin.shape)
    v_print(args.verbose, 1, bin)
    v_print(args.verbose, 1, adjbin)
    v_print(args.verbose, 1, adjust)

    # specify chr_size  ### these are different to the defaul once from the original script, these where hardcoded back then
    # here we have them dependent on the genome version we use, right now T2T-CHM13
    # pylint: disable=maybe-no-member
    samfile = pysam.AlignmentFile(args.bam, "rb", threads=args.threads)
    p = samfile.check_index()
    sam_list = samfile.get_index_statistics()
    selected_genome_info, FLAG = check_genome_version(
        sam_list, default=args.default_ucsc)
    chr_size = list()
    for chr in range(0, len(selected_genome_info.items())):
        chr_name = list(list(selected_genome_info.items())
                        [chr][1].items())[1][1]
        chr_length = list(list(selected_genome_info.items())
                          [chr][1].items())[3][1]
        v_print(args.verbose, 1, f"name: {chr_name} length: {chr_length}")
        chr_size.append(chr_length/int(bin_size))
        v_print(args.verbose, 1, f"{chr_size[chr]}")

    # recalculate adjbin so it is not integer
    #
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):  #
            if (adjust.iat[loopC, loopA] != 0):
                # v_print(args.verbose, 1,f"bin.iat[loopA,loopC]: {type(bin.iat[loopA,loopC])}")
                # v_print(args.verbose, 1,f"Acorr_factor: {type(Acorr_factor)}")
                # v_print(args.verbose, 1,f"Acorr_factor: {Acorr_factor}")
                # v_print(args.verbose, 1,f"adjust.iat[loopA,loopC]: {type(adjust.iat[loopA,loopC])}")
                # v_print(args.verbose, 1,f"1000.0: {type(1000.0)}")
                # v_print(args.verbose, 1,f"result: {float(Acorr_factor)*1000.0}")
                # v_print(args.verbose, 1,f"adjust.iat[loopA,loopC]:")

                adjbin_2.iat[loopC, loopA] = int(
                    (bin.iat[loopC, loopA]*float(Acorr_factor)*1000)/adjust.iat[loopC, loopA]+0.5)
                if (abs(adjbin_2.iat[loopC, loopA]-adjbin.iat[loopC, loopA]) > 1):
                    v_print(args.verbose, 1, "[Warning]:\n")
                    v_print(args.verbose, 1, f"{file_Name}  and")
                    v_print(args.verbose, 1, f"{quality_file_Name}")
                    v_print(args.verbose, 1, f"are not from the same dataset.\n")

                adjbin.iat[loopC, loopA] = adjbin_2.iat[loopC, loopA]
            else:
                adjbin.iat[loopC, loopA] = 0
    v_print(args.verbose, 1, adjbin)
    # perform some file validation
    #
    bin_recalc_total_hits = 0
    adjust_recalc_total_hits = 0
    for loopC in range(1, 23+1):
        # for loopA in range(0, int(chr_size[loopC])):
        for loopA in range(0, number_bins+1):
            if (adjust.iat[loopC, loopA] != 0):
                #
                bin_recalc_total_hits = bin_recalc_total_hits + \
                    int(bin.iat[loopC, loopA])
                adjust_recalc_total_hits = adjust_recalc_total_hits + \
                    int(adjust.iat[loopC, loopA])
                #
    #
    v_print(args.verbose, 1, f"bin_recalc_total_hits!=bin_total_hits?")
    v_print(args.verbose, 1, f"bin_recalc_total_hits: {bin_recalc_total_hits}")
    v_print(args.verbose, 1, f"bin_total_hits:  {bin_total_hits}")

    v_print(args.verbose, 1, f"adjust_recalc_total_hits!=adjust_total_hits")
    v_print(args.verbose, 1,
            f"adjust_recalc_total_hits: {adjust_recalc_total_hits}")
    v_print(args.verbose, 1, f"adjust_total_hits:  {adjust_total_hits}")

    if (bin_recalc_total_hits != bin_total_hits):
        v_print(args.verbose, 1, "[Warning]:\n")
        v_print(args.verbose, 1, f"{file_Name}  and")
        v_print(args.verbose, 1, f"{quality_file_Name}")
        v_print(args.verbose, 1,
                f"have not the same amount of adjusted total hits\n")
        # exit()

    if (adjust_recalc_total_hits != adjust_total_hits):
        v_print(args.verbose, 1, "[Warning]:\n")
        v_print(args.verbose, 1, f"{file_Name}  and")
        v_print(args.verbose, 1, f"{quality_file_Name}")
        v_print(args.verbose, 1,
                f" have not the same amount of adjusted total hits\n")
        # exit()
    # start calculations to determine background
    #
    # establish value of adjbin counts that corresponds to background
    #
    #
    all_non0_adjbin = list()
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            # eliminates adjbin values = 0 (i.e. no data recorded at that position)
            if (adjust.iat[loopC, loopA] != 0 and adjbin.iat[loopC, loopA] > 0):
                all_non0_adjbin.append(adjbin.iat[loopC, loopA])

    v_print(args.verbose, 1, f"all_non0_adjbin: {all_non0_adjbin[1:100]}")
    #
    #
    #
    # method to calculate background
    #    sort all values
    #    plot values at every percentage fraction
    #      identify the percentage, where there is a significant increase in values
    #
    # example of representative results:
    #
    #
    # Actual sorted adjbin values from 9-99 percentiles:
    # 25 27 29 31 33 34 36 39 41 43 45 47 49 52 54 56 59    (25)
    # 61 64 66 69 72 75 78 81 84 87 91 94 98 102 106    (40)
    # 110 115 119 124 130 135 141 147 153 160 168 176 184 193 203    (55)
    # 213 225 237 250 264 279 296 315 335 357 381 408 437 471 506    (70)
    # 546 588 636 688 747 815 890 972 1063 1167 1287 1415 1561 1730 1925    (85)
    # 2140 2395 2683 3025 3422 3911 4487 5173 5995 7046 8395 10135 12856 17464    (99)
    #
    # Actual sorted adjbin values from 5-95 percentiles:
    # 17 19 21 23 25 27 29 31 33 34 36 39 41 43 45 47 49    (21)
    # 52 54 56 59 61 64 66 69 72 75 78 81 84 87 91    (36)
    # 94 98 102 106 110 115 119 124 130 135 141 147 153 160 168    (51)
    # 176 184 193 203 213 225 237 250 264 279 296 315 335 357 381    (66)
    # 408 437 471 506 546 588 636 688 747 815 890 972 1063 1167 1287    (81)
    # 1415 1561 1730 1925 2140 2395 2683 3025 3422 3911 4487 5173 5995 7046    (95)
    #
    # Difference of sorted adjbin value at percentile x minus at percentile x-4, for percentiles 9-99 (=diff[x]):
    # 8 8 8 8 8 8 8 8 8 8 8 8 9 9 9 9 9    (25)
    # 9 10 10 10 11 11 11 12 12 13 13 14 14 15 16    (40)
    # 16 17 17 18 19 20 21 23 24 25 27 29 31 33 35    (55)
    # 38 41 44 47 51 54 59 65 71 77 85 93 102 114 124    (70)
    # 137 151 166 183 201 227 253 283 316 352 397 443 498 563 638    (85)
    # 725 834 953 1100 1282 1516 1803 2148 2573 3135 3908 4961 6861 10418    (99)
    #
    # Function to define background: (diff[x]-diff[x-4])/(diff[x]-diff[x-4])/2*100  for percentiles 9-99:
    # -0.5 -0.2 -0.2 -0.2 -0.6 0.1 0.5 1.2 1.3 1.5 1.6 1.3 1.9 1.8 2 1.8 1.6    (25)
    # 1.8 1.9 2.6 2.9 3.2 3.2 3.1 3 3.1 3.7 3.3 3.5 3.6 3.4 4.3    (40)
    # 4.4 4.1 4.2 3.8 4.2 4.9 5.1 5.5 5.5 5.6 5.8 6.1 6.5 6.5 6.8    (55)
    # 6.7 7 7.4 7.1 7.5 7 7.1 8 8.2 8.8 9.1 8.8 9 9.6 9.5    (70)
    # 9.6 9.6 9.2 9.5 9.4 10 10.5 10.8 11.1 10.8 11.1 11 11.1 11.5 11.6    (85)
    # 12.1 12.6 12.9 13.3 13.9 14.5 15.4 16.1 16.7 17.4 18.4 19.8 22.7 26.9    (99)
    #
    #
    # sort arrays to find background value for each sample
    #
    #
    v_print(args.verbose, 1, f"Step 1: Determine background (noise) adjbin value:\n")
    #
    # we want to return the sorted list not sort in place (.sort())
    sort_all_non0_adjbin = sorted(all_non0_adjbin)
    # v_print(args.verbose, 1,f"sort_all_non0_adjbin: {sort_all_non0_adjbin}")
    v_print(args.verbose, 1,
            f"sort_all_non0_adjbin: {sort_all_non0_adjbin[1:100]}")
    v_print(args.verbose, 1,
            f"sort_all_non0_adjbin: {type(sort_all_non0_adjbin)}")
    #
    # calculates actual values (just to see values)
    v_print(args.verbose, 1, f"Actual sorted adjbin values from 9-99 percentiles:")
    for loopC in range(9, 100):
        # number_percent_now = int($#sort_all_non0_adjbin*$loopC/100); original perl
        # length of list element * percent loopC / 100
        number_percent_now = int(len(sort_all_non0_adjbin)*loopC/100)
        # v_print(args.verbose, 1,f" npcn:{number_percent_now}")
        now = int(sort_all_non0_adjbin[number_percent_now])
        for_print = now
        v_print(args.verbose, 1, f"{for_print}", end=' ')
        if (loopC == 25 or loopC == 40 or loopC == 55 or loopC == 70 or loopC == 85 or loopC == 99):
            v_print(args.verbose, 1, f"   ({loopC})")
    # calculates actual values at -4 position (just to see values)
    v_print(args.verbose, 1, f"Actual sorted adjbin values from 5-95 percentiles:")
    for loopC in range(9, 100):
        # v_print(args.verbose, 1,loopC)
        # number_percent_now = int($#sort_all_non0_adjbin*$loopC/100); original perl
        # length of list element * percent loopC / 100
        number_percent_now = int(len(sort_all_non0_adjbin)*(loopC-4)/100)
        # v_print(args.verbose, 1,f" npcn:{number_percent_now}")
        now = int(sort_all_non0_adjbin[number_percent_now])
        for_print = now
        v_print(args.verbose, 1, f"{for_print}", end=' ')
        if (loopC == 25 or loopC == 40 or loopC == 55 or loopC == 70 or loopC == 85 or loopC == 99):
            v_print(args.verbose, 1, f"   ({loopC-4})")
    # calculates first difference (just to see values)
    v_print(args.verbose, 1,
            f"Difference of sorted adjbin value at percentile x minus at percentile x-4, for percentiles 9-99 (=diff[x]):")
    for loopC in range(9, 100):
        number_percent_pre = int(len(sort_all_non0_adjbin)*(loopC-4)/100)
        number_percent_now = int(len(sort_all_non0_adjbin)*loopC/100)
        diff_now = (
            sort_all_non0_adjbin[number_percent_now]-sort_all_non0_adjbin[number_percent_pre])
        for_print = int(diff_now)
        v_print(args.verbose, 1, f"{for_print}", end=' ')
        if (loopC == 25 or loopC == 40 or loopC == 55 or loopC == 70 or loopC == 85 or loopC == 99):
            v_print(args.verbose, 1, f"   ({loopC})")
    find_back_sec_diff = list()
    for i in range(1, 100+1):
        # In each iteration, add an empty list to the main list
        find_back_sec_diff.append('')
    v_print(args.verbose, 1, len(find_back_sec_diff))
    # much better!!!
    v_print(args.verbose, 1, find_back_sec_diff)
    # calculates function to define background
    find_back_sec_diff = list()
    for i in range(1, 100+1):
        # In each iteration, add an empty list to the main list
        find_back_sec_diff.append('')
    v_print(args.verbose, 1,
            "Function to define background: (diff[x]-diff[x-4])/(diff[x]+diff[x-4])/2*100  for percentiles 9-99:")
    for loopC in range(9, 100):
        number_percent_pre2 = int(len(sort_all_non0_adjbin)*(loopC-8)/100)
        number_percent_pre = int(len(sort_all_non0_adjbin)*(loopC-4)/100)
        number_percent_now = int(len(sort_all_non0_adjbin)*loopC/100)
        diff_pre = (sort_all_non0_adjbin[number_percent_pre] -
                    sort_all_non0_adjbin[number_percent_pre2])
        diff_now = (
            sort_all_non0_adjbin[number_percent_now]-sort_all_non0_adjbin[number_percent_pre])
        if (diff_now+diff_pre == 0):
            find_back_sec_diff[loopC] = 0
        else:
            find_back_sec_diff[loopC] = int(
                (diff_now-diff_pre)/(diff_now+diff_pre)/2*1000)/10

        v_print(args.verbose, 1, f"{find_back_sec_diff[loopC]}", end=" ")
        if (loopC == 25 or loopC == 40 or loopC == 55 or loopC == 70 or loopC == 85 or loopC == 99):
            v_print(args.verbose, 1, f"   ({loopC})")

    v_print(args.verbose, 1, find_back_sec_diff)
    # stringency: avoids artifacts at low loopC or makes sure that slope continues to increase
    #   finds range of loopC values to examine for minimum SD
    #

    percentile_baseline_cutoff_low = 0
    for loopC in range(12, 90):
        mov_ave_find_back_sec_diff_now = find_back_sec_diff[loopC-3] + \
            find_back_sec_diff[loopC-2] + \
            find_back_sec_diff[loopC-1]+find_back_sec_diff[loopC]
        # v_print(args.verbose, 1,f"mov_ave_find_back_sec_diff_now {mov_ave_find_back_sec_diff_now}")
        mov_ave_find_back_sec_diff_after = find_back_sec_diff[loopC+1] + \
            find_back_sec_diff[loopC+2] + \
            find_back_sec_diff[loopC+3]+find_back_sec_diff[loopC+4]
        # v_print(args.verbose, 1,f"mov_ave_find_back_sec_diff_after {mov_ave_find_back_sec_diff_after}")
        if (mov_ave_find_back_sec_diff_now >= 8 and (loopC >= 21 or mov_ave_find_back_sec_diff_after >= 8)):
            v_print(args.verbose, 1, f"if (mov_ave_find_back_sec_diff_now>=8 and (loopC>=21 or mov_ave_find_back_sec_diff_after>=8) ):")
            v_print(args.verbose, 1,
                    f"mov_ave_find_back_sec_diff_now: {mov_ave_find_back_sec_diff_now}")
            v_print(args.verbose, 1, f"loopC: {loopC}")
            v_print(args.verbose, 1,
                    f"mov_ave_find_back_sec_diff_after: {mov_ave_find_back_sec_diff_after}")
            percentile_baseline_cutoff_low = loopC-11
            v_print(args.verbose, 1,
                    f"Percentile at low baseline cutoff: {percentile_baseline_cutoff_low}")
            number_percent_now = int(len(sort_all_non0_adjbin)*(loopC-11)/100)
            adjbin_baseline_cutoff_low = sort_all_non0_adjbin[number_percent_now]
            v_print(args.verbose, 1,
                    f"Baseline of adjbin data values at low cutoff: {adjbin_baseline_cutoff_low}\n")
            break  # loopC=120 in pearl as a break statement gets ignored by python

    if (percentile_baseline_cutoff_low == 0):
        v_print(args.verbose, 1,
                "******** WARNING: FAILED TO FIND baseline_cutoff_low EXITING ************\n")
        # exit();
    #
    percentile_baseline_cutoff_high = 0
    for loopC in range(12, 90):
        # v_print(args.verbose, 1,f"loopC: {loopC}")
        # v_print(args.verbose, 1,f"mov_ave_find_back_sec_diff_now {mov_ave_find_back_sec_diff_now}")
        # v_print(args.verbose, 1,f"mov_ave_find_back_sec_diff_after {mov_ave_find_back_sec_diff_after}")

        mov_ave_find_back_sec_diff_now = find_back_sec_diff[loopC-3] + \
            find_back_sec_diff[loopC-2] + \
            find_back_sec_diff[loopC-1]+find_back_sec_diff[loopC]
        mov_ave_find_back_sec_diff_after = find_back_sec_diff[loopC+1] + \
            find_back_sec_diff[loopC+2] + \
            find_back_sec_diff[loopC+3]+find_back_sec_diff[loopC+4]
        if (mov_ave_find_back_sec_diff_now >= 32 and (loopC >= 41 or mov_ave_find_back_sec_diff_after >= 32)):
            percentile_baseline_cutoff_high = loopC-11
            v_print(args.verbose, 1,
                    f"mov_ave_find_back_sec_diff_now>=32 and (loopC>=41 or mov_ave_find_back_sec_diff_after>=32)")
            v_print(args.verbose, 1,
                    f"mov_ave_find_back_sec_diff_now: {mov_ave_find_back_sec_diff_now}")
            v_print(args.verbose, 1, f"loopC: {loopC}")
            v_print(args.verbose, 1,
                    f"mov_ave_find_back_sec_diff_after: {mov_ave_find_back_sec_diff_after}")
            v_print(args.verbose, 1,
                    f"Percentile at high baseline cutoff: {percentile_baseline_cutoff_high}")
            number_percent_now = int(len(sort_all_non0_adjbin)*(loopC-11)/100)
            adjbin_baseline_cutoff_high = sort_all_non0_adjbin[number_percent_now]
            v_print(args.verbose, 1,
                    f"Baseline of adjbin data values at high cutoff: {adjbin_baseline_cutoff_high}\n")
            break  # loopC=120 in pearl as a break statement gets ignored by python

    if (percentile_baseline_cutoff_high == 0):
        v_print(args.verbose, 1, f"******** WARNING: FAILED TO FIND baseline_cutoff_high SAMPLE MAY HAVE NO PEAKS ************\n")
        percentile_baseline_cutoff_high = percentile_baseline_cutoff_low+20
        if (percentile_baseline_cutoff_high > 89):
            percentile_baseline_cutoff_high = percentile_baseline_cutoff_low+1

        adjbin_baseline_cutoff_high = adjbin_baseline_cutoff_low*1.1

    min_diff_mean_median = adjbin_baseline_cutoff_high
    adjbin_baseline_mean_at_min = 0
    adjbin_baseline_SD_at_min = 0
    used_percentile_baseline_at_min = 0
    nAdjBin_at_min = 0
    adjbin_baseline_cutoff_at_min = 0
    percentile_baseline_cutoff_at_min = 0
    #
    v_print(args.verbose, 1, f"Finds cutoff (=baseline) of adjbin value, below which a genomic bin is considered to be background noise")
    v_print(args.verbose, 1, f"Baseline is within low and high baseline cutoffs and selected when difference between mean and median")
    v_print(args.verbose, 1, f"   baseline values is minimized")
    v_print(args.verbose, 1,
            f"%      Median   Mean        Cutoff      SD_of_mean %    Diff_mean_median")

    # $percentile_baseline_cutoff_low   is equal to  $loopC-11  above
    for loopC in range(percentile_baseline_cutoff_low, percentile_baseline_cutoff_high+1):
        # called  $number_percent_now  above
        index_number_for_baseline_cutoff = int(
            len(sort_all_non0_adjbin)*loopC/100)
        index_number_for_baseline_median = int(
            len(sort_all_non0_adjbin)*loopC*0.5/100)
        adjbin_baseline_median = int(
            sort_all_non0_adjbin[index_number_for_baseline_median]*1000+0.5)/1000
        adjbin_baseline_cutoff = sort_all_non0_adjbin[index_number_for_baseline_cutoff]
        #
        # calculate mean value and SD of all adjbin values that are equal to or below  $adjbin_baseline_cutoff
        #
        SAdjBin = 0
        SSAdjBin = 0
        nAdjBin = 0
        for loopD in range(0, len(sort_all_non0_adjbin)+1):
            if (sort_all_non0_adjbin[loopD] <= adjbin_baseline_cutoff):
                SAdjBin = SAdjBin+sort_all_non0_adjbin[loopD]
                SSAdjBin = SSAdjBin + \
                    sort_all_non0_adjbin[loopD]*sort_all_non0_adjbin[loopD]
                nAdjBin += 1
            else:
                # v_print(args.verbose, 1,f"INLOOP {used_percentile_baseline}")
                used_percentile_baseline = int(
                    loopD/len(sort_all_non0_adjbin)*100)
                # aw: loopD=len(sort_all_non0_adjbin)+100 fucked up perl to python break case
                break

        # v_print(args.verbose, 1,f"Percentile used to calculate SD of baseline\n (should be equal to percentile at baseline): {used_percentile_baseline}")
        #
        adjbin_baseline_mean = int(SAdjBin/nAdjBin*1000+0.5)/1000
        adjbin_baseline_SD = int(
            math.sqrt((SSAdjBin-SAdjBin*SAdjBin/nAdjBin)/(nAdjBin-1))*1000+0.5)/1000
        # v_print(args.verbose, 1,f"Mean baseline of adjbin data values: {adjbin_baseline_mean}")
        # v_print(args.verbose, 1,f"ANOVA-based SD of mean baseline of adjbin data values: {adjbin_baseline_SD}")
        # v_print(args.verbose, 1,f"Number of adjbin data values used to calculate mean and SD: {nAdjBin}\n\n")
        #
        diff_median_mean = adjbin_baseline_median-adjbin_baseline_mean
        if (abs(diff_median_mean) <= min_diff_mean_median):
            min_diff_mean_median = abs(diff_median_mean)
            adjbin_baseline_mean_at_min = adjbin_baseline_mean
            adjbin_baseline_SD_at_min = adjbin_baseline_SD
            used_percentile_baseline_at_min = used_percentile_baseline
            nAdjBin_at_min = nAdjBin
            adjbin_baseline_cutoff_at_min = adjbin_baseline_cutoff
            percentile_baseline_cutoff_at_min = loopC

        #
        diff_median_mean = int(
            (adjbin_baseline_median-adjbin_baseline_mean)*1000+0.5)/1000
        adjbin_baseline_cutoff = int(adjbin_baseline_cutoff*1000+0.5)/1000
        v_print(args.verbose, 1, f"{loopC}    ", end=" ")
        v_print(args.verbose, 1, f"{adjbin_baseline_median} ", end=" ")
        v_print(args.verbose, 1, f"{adjbin_baseline_mean}    ", end=" ")
        v_print(args.verbose, 1, f"{adjbin_baseline_cutoff}   ", end=" ")
        v_print(args.verbose, 1,
                f"{adjbin_baseline_SD}    {used_percentile_baseline}    {diff_median_mean}")
        #

    #
    adjbin_baseline_mean = adjbin_baseline_mean_at_min
    adjbin_baseline_SD = adjbin_baseline_SD_at_min
    used_percentile_baseline = used_percentile_baseline_at_min
    nAdjBin = nAdjBin_at_min
    adjbin_baseline_cutoff = adjbin_baseline_cutoff_at_min
    percentile_baseline_cutoff = percentile_baseline_cutoff_at_min
    #
    v_print(args.verbose, 1,
            f"Baseline of adjbin data values (cutoff): {adjbin_baseline_cutoff}")
    v_print(args.verbose, 1,
            f"Percentile at baseline: {percentile_baseline_cutoff}")
    v_print(args.verbose, 1,
            f"Percentile used to calculate SD of baseline\n (should be equal to percentile at baseline): {used_percentile_baseline}")
    v_print(args.verbose, 1,
            f"Mean baseline of adjbin data values: {adjbin_baseline_mean}")
    v_print(args.verbose, 1,
            f"ANOVA-based SD of mean baseline of adjbin data values: {adjbin_baseline_SD}")
    v_print(args.verbose, 1,
            f"Number of adjbin data values used to calculate mean and SD: {nAdjBin}\n")
    # find SD of adjbin noise counts corresponding to different levels of adjust counts
    #
    #
    #
    non0_adjust_noise = list()
    # non0_adjust_noise.append(adjust.iat[1,0])# index starts at 1 again || these seem to be all noise values smaller than the baseline cutoff
    v_print(args.verbose, 1,
            f"adjbin_baseline_cutoff {adjbin_baseline_cutoff}")
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            # eliminates adjbin values = 0 (i.e. no data recorded at that position)
            if (int(adjust.iat[loopC, loopA]) != 0):

                # if(adjbin.iat[loopA,loopC]<=adjbin_baseline_cutoff and adjbin.iat[loopA,loopC] > 0): # we still have 0.0 values in the noise so should i add an additional request to skip 0 values? #TODO
                if (adjbin.iat[loopC, loopA] <= adjbin_baseline_cutoff):
                    # if len(non0_adjust_noise)<= 100: v_print(args.verbose, 1,f"int {int(adjust.iat[loopA,loopC])} non int {adjust.iat[loopA,loopC]}")
                    non0_adjust_noise.append(adjust.iat[loopC, loopA])

    v_print(args.verbose, 1, non0_adjust_noise[0:100])
    v_print(args.verbose, 1, non0_adjust_noise[1])
    v_print(args.verbose, 1, len(non0_adjust_noise))
    ################ adjust linit ###################
    number_split_adj_read = 200  # how many bins we will split the adjust reads into
    adjust_limit = list()
    # adjust_limit.append() # perl index starts at 1 and the list at zero
    #
    # we are still sorting the noise list, here might be an issue with zeros?
    # we want to return the sorted list not sort in place (.sort())
    sort_non0_adjust_noise = sorted(non0_adjust_noise)
    v_print(args.verbose, 1,
            f"sort_non0_adjust_noise {len(sort_non0_adjust_noise)}")
    # v_print(args.verbose, 1,f" {sort_non0_adjust_noise[96729]}")
    # aw: we are starting at index 1 in perl for loopE so we need to acount for it, index 0 is 0
    adjust_limit.append(0)

    # aw: perl equivlent sort_non0_adjust_noise=sort { $a <=> $b } @non0_adjust_noise;
    # aw: normally we could deal with starting at one but since lists start at zero and we cant mimic it starting in one as perl is capable we need to reduce the index by one
    for loopE in range(1, number_split_adj_read+1):
        index = int(len(sort_non0_adjust_noise)*loopE/number_split_adj_read)
        v_print(args.verbose, 1, index)
        adjust_limit.append(sort_non0_adjust_noise[index-1])
        v_print(args.verbose, 1,
                f"loopE {loopE} adjust_limit[loopE] {adjust_limit[loopE]}")

    v_print(args.verbose, 1, sort_non0_adjust_noise[0:100])
    v_print(args.verbose, 1, len(sort_non0_adjust_noise))
    v_print(args.verbose, 1, adjust_limit)
    v_print(args.verbose, 1, len(adjust_limit))
    v_print(args.verbose, 1, "done")
    ##################################### list adjbin noise ##############################

    list_adjbin_noise = [[]] * number_split_adj_read
    for elem in list_adjbin_noise:
        # v_print(args.verbose, 1,id(elem))
        pass

    # all list are actually only reference!!! ufff
    v_print(args.verbose, 1, list_adjbin_noise[2].append(43))
    # we need own instances!!!

    # Create an empty list
    list_adjbin_noise = []
    # Iterate over a sequence of numbers from 0 to 4
    for i in range(1, number_split_adj_read+1):
        # In each iteration, add an empty list to the main list
        list_adjbin_noise.append([])

    v_print(args.verbose, 1, list_adjbin_noise)
    for elem in list_adjbin_noise:
        v_print(args.verbose, 1, id(elem), end="; ")

    v_print(args.verbose, 1, len(list_adjbin_noise))
    # much better!!!
    ############## index list ##################

    index_list = list()
    index_list.append("")
    adjust_limit[0] = 0
    # print "\n";
    #
    for loopE in range(1, number_split_adj_read+1):
        index_list.append(0)
    v_print(args.verbose, 1, index_list)
    v_print(args.verbose, 1, len(index_list))
    # aw: this is TOTALLY WRONG -> it only creates reference instancesin the sublist, do you change one value in one entrie it changes for ALL
    #   list_adjbin_noise = [[]] *number_split_adj_read

    # aw: correct way
    # Create an empty list
    list_adjbin_noise = []
    # Iterate over a sequence of numbers
    for i in range(0, number_split_adj_read+1):
        # In each iteration, add an empty list to the main list
        list_adjbin_noise.append([])
    v_print(args.verbose, 1, len(list_adjbin_noise))
    v_print(args.verbose, 1, list_adjbin_noise)
    # v_print(args.verbose, 1,list_adjbin_noise)

    # v_print(args.verbose, 1,list_adjbin_noise)
    # v_print(args.verbose, 1,number_split_adj_read)

    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            if (adjust.iat[loopC, loopA] != 0):
                if (adjbin.iat[loopC, loopA] <= adjbin_baseline_cutoff):
                    # v_print(args.verbose, 1,"if (adjbin.iat[loopA,loopC]<=adjbin_baseline_cutoff):")
                    for loopE in range(1, number_split_adj_read+1):
                        # v_print(args.verbose, 1,f"{loopE}",end =" ")
                        if (adjust.iat[loopC, loopA] > adjust_limit[loopE-1] and adjust.iat[loopC, loopA] <= adjust_limit[loopE]):
                            # v_print(args.verbose, 1,"if (adjust.iat[loopA,loopC]>adjust_limit[loopE-1] and adjust.iat[loopA,loopC]<=adjust_limit[loopE]):")
                            # v_print(args.verbose, 1,f"adjust.iat[loopA,loopC] {adjust.iat[loopA,loopC]} > adjust_limit[loopE-1] {adjust_limit[loopE-1]} and adjust.iat[loopA,loopC] {adjust.iat[loopA,loopC]} <= adjust_limit[loopE] {adjust_limit[loopE]}")
                            if loopE % 200 == 0 and index_list[loopE] % 10 == 0:
                                v_print(args.verbose, 1,
                                        f"idx list value: {index_list[loopE]}, loopE {loopE} ")
                                v_print(args.verbose, 1,
                                        f"Value to save: {adjbin.iat[loopC,loopA]}")
                            list_adjbin_noise[loopE].append(
                                adjbin.iat[loopC, loopA])
                            # aw: this index list is only for appending to the list in perl index_list[loopE]=index_list[loopE]+1
                            #     it should be enough to only use loopE for finding the correct bin and just append the values after each other
                            #     but it can track counts of list elements in each list, as its used in later code
                            index_list[loopE] = index_list[loopE]+1
                            break  # aw: this should be a break in perl?: loopE=number_split_adj_read+1

                    # v_print(args.verbose, 1," ede")

    # v_print(args.verbose, 1,list_adjbin_noise)
    # v_print(args.verbose, 1,index_list)
    v_print(args.verbose, 1, "done")
    #
    # for ($loopE=1; $loopE<=$number_split_adj_read; $loopE++) {
    #    print "$index_list[$loopE]\n";
    # }
    # print "\n";
    #
    # calculate mean value and SD of all adjbin values that are equal to or below  $adjbin_baseline_cutoff
    #    according to number of adjust reads (adjust reads divided in bins using  $loopE )
    #
    start_loopE = 1
    adjbin_baseline_mean_split_adj_read = list()
    adjbin_baseline_SD_split_adj_read = list()
    for loopE in range(0, number_split_adj_read+1):
        adjbin_baseline_mean_split_adj_read.append("")
        adjbin_baseline_SD_split_adj_read.append("")

    v_print(args.verbose, 1,
            f"number_split_adj_read {number_split_adj_read}; number_split_adj_read+1 {number_split_adj_read+1}")

    for loopE in range(1, number_split_adj_read+1):
        SAdjBin = 0
        SSAdjBin = 0
        nAdjBin = index_list[loopE]
        # v_print(args.verbose, 1,f"index_list[loopE] {loopE}")
        for loopD in range(1, index_list[loopE]):
            # v_print(args.verbose, 1,f"index_list[loopE] {list_adjbin_noise}")
            SAdjBin = SAdjBin+list_adjbin_noise[loopE][loopD]
            SSAdjBin = SSAdjBin + \
                list_adjbin_noise[loopE][loopD]*list_adjbin_noise[loopE][loopD]

        #
        if (nAdjBin == 0 or nAdjBin == 1):
            adjbin_baseline_mean_split_adj_read[loopE] = 0
            adjbin_baseline_SD_split_adj_read[loopE] = 0
        else:
            adjbin_baseline_mean_split_adj_read[loopE] = int(
                SAdjBin/nAdjBin*1000+0.5)/1000
            adjbin_baseline_SD_split_adj_read[loopE] = int(
                math.sqrt((SSAdjBin-SAdjBin*SAdjBin/nAdjBin)/(nAdjBin-1))*1000+0.5)/1000

        if (adjbin_baseline_mean_split_adj_read[loopE] == 0 or adjbin_baseline_SD_split_adj_read[loopE] == 0):
            v_print(args.verbose, 1, f"start loop E was increased")
            start_loopE = loopE+1
        v_print(args.verbose, 1,
                f"Mean baseline of adjbin data values: {adjbin_baseline_mean_split_adj_read[loopE]}")
        v_print(args.verbose, 1,
                f"ANOVA-based SD of mean baseline of adjbin data values: {adjbin_baseline_SD_split_adj_read[loopE]}")
        v_print(args.verbose, 1,
                f"Number of adjbin data values used to calculate mean and SD: {nAdjBin}\n")
        #

    # v_print(args.verbose, 1,f"{adjbin_baseline_mean_split_adj_read}")
    v_print(args.verbose, 1, f"start loop E: {start_loopE}")
    v_print(args.verbose, 1, "Done")
    #
    v_print(args.verbose, 1, f"start_loopE: {start_loopE}")
    v_print(args.verbose, 1, f"number_split_adj_read: {number_split_adj_read}")
    v_print(args.verbose, 1, f"Bin\tNumber of adjust reads\tAdjbin Baseline SD\n")
    linx = list()
    liny = list()
    logx = list()
    logy = list()
    for loopE in range(0, number_split_adj_read+1):
        linx.append("")
        liny.append("")
        logx.append("")
        logy.append("")

    for loopE in range(start_loopE, number_split_adj_read+1):
        v_print(args.verbose, 1,
                f"{loopE}\t\t{adjust_limit[loopE]}\t{adjbin_baseline_SD_split_adj_read[loopE]}")
        linx[loopE] = adjust_limit[loopE]
        liny[loopE] = adjbin_baseline_SD_split_adj_read[loopE]
        logx[loopE] = math.log(adjust_limit[loopE])
        logy[loopE] = math.log(adjbin_baseline_SD_split_adj_read[loopE])

    # print "\n";
    #
    v_print(args.verbose, 1, "Done")
    v_print(args.verbose, 1, logx)
    # assume data fit a power curve of the type:  y = a * x^b
    #
    # determine R2 and slope for multiple start positions of power curve, each with a fixed number of data points = $number_points_line
    #
    # conversion of power curve to linear:  log(y) = log(a) + b log(x)  =>   Y = A + b X
    #
    #
    #
    # how many bins we will split the adjust reads into
    number_points_line = int(number_split_adj_read/4)
    if (number_points_line < 10):
        v_print(args.verbose, 1,
                f"Number_points_line = {number_points_line} too small. Increase number_split_adj_read in Line above\n")
        # exit;

    #
    # create lists with zero, only appending may fail later on with references these as indices
    SLOPE_log_eval = list()
    R2 = list()
    # its <=  ... +1 in perl script -> <= is +1 for us so its +2
    for loopE in range(0, number_split_adj_read-number_points_line+2):
        SLOPE_log_eval.append(0)
        R2.append(0)

    v_print(args.verbose, 1, f"number_split_adj_read {number_split_adj_read}")
    v_print(args.verbose, 1, f"number_points_line {number_points_line}")
    for loopE in range(start_loopE, number_split_adj_read-number_points_line+2):
        #
        SX = 0.0
        SSX = 0.0
        SY = 0.0
        SSY = 0.0
        SXY = 0.0
        n = number_points_line
        #
        for loopD in range(1, number_points_line+1):
            loop_now = loopE+loopD-1
            # v_print(args.verbose, 1,f"loop_now {loop_now}")
            # v_print(args.verbose, 1,f"logx[loop_now] {logx}")
            # v_print(args.verbose, 1,f"SX=SX+logx[loop_now] :SX={SX}+{logx[loop_now]}  \n")
            SX = SX+logx[loop_now]
            # v_print(args.verbose, 1,f"SSX=SSX+logx[loop_now]*logx[loop_now] :SSX={SSX}+{logx[loop_now]}*{logx[loop_now]}  \n")
            SSX = SSX+(logx[loop_now]*logx[loop_now])
            SY = SY+logy[loop_now]
            SSY = SSY+(logy[loop_now]*logy[loop_now])
            SXY = SXY+(logx[loop_now]*logy[loop_now])

        #
        # calculations
        #
        SXSY = SX*SY
        SXMYM = SXY-(SXSY/n)
        SXMXM = SSX-(SX*SX/n)
        SXM2 = SXMXM
        SYMYM = SSY-(SY*SY/n)
        SLOPE = SXMYM/SXMXM
        INTER = (SY-SX*SLOPE)/n
        SSESLOPE = SYMYM-SLOPE*SLOPE*SXMXM
        # v_print(args.verbose, 1,f"SSESLOPE: {SSESLOPE}")
        SDSLOPE = math.sqrt(SSESLOPE/(n-2)/SXMXM)
        TTEST = SLOPE/SDSLOPE
        DF = n-2
        SLOPE_log_eval[loopE] = SLOPE
        # calculate R2
        SST = SSY
        SS_Regression = SLOPE*SXMYM
        SS_Residual = SST-SS_Regression-SY*SY/n
        SSM = SY*SY/n
        R2[loopE] = SS_Regression/(SS_Residual+SS_Regression)
        #

    #
    # evaluate power curve and define start position
    #
    max_neg_SLOPE = 0
    max_R2 = 0
    loopE_at_max_R2 = 0
    loopE_at_max_neg_SLOPE = 0
    for loopE in range(start_loopE, number_split_adj_read-number_points_line+2):
        if (R2[loopE] > max_R2):
            max_R2 = R2[loopE]
            loopE_at_max_R2 = loopE
        if (SLOPE_log_eval[loopE] < max_neg_SLOPE):
            max_neg_SLOPE = SLOPE_log_eval[loopE]
            loopE_at_max_neg_SLOPE = loopE

    v_print(args.verbose, 1, f"\nStep 2: Calculate SD at each genomic bin using background noise data and sequence coverage at each genomic bin:\n")
    v_print(args.verbose, 1, f"Define power curve (adjust_reads vs adjbin_SD)")
    v_print(args.verbose, 1,
            f"Number of data segments (points defining curve): {number_split_adj_read}\n")
    v_print(args.verbose, 1, f"loopE_at_max_R2): {loopE_at_max_R2}\n")
    v_print(args.verbose, 1, f"SLOPE_log_eval): {SLOPE_log_eval}\n")
    v_print(args.verbose, 1,
            f"Max R2:          {max_R2}      Segment of max R2:  {loopE_at_max_R2}    Slope at this position:  {SLOPE_log_eval[loopE_at_max_R2]}")
    v_print(args.verbose, 1,
            f"Max neg slope:  {max_neg_SLOPE}      Segment of max R2:  {loopE_at_max_neg_SLOPE}    R2 at this position:  {R2[loopE_at_max_neg_SLOPE]}")

    #
    if (loopE_at_max_neg_SLOPE == 0):
        v_print(args.verbose, 1,
                "\nNo negative slope, Curve not as expected, Exiting\n\n")
        # exit()
    #
    start_power_curve = 0

    # again # its <=  ... +1 in perl script -> <= is +1 therefore here its +2
    v_print(args.verbose, 1, f"startloope: {start_loopE}")
    v_print(args.verbose, 1, f"number_split_adj_read: {number_split_adj_read}")
    v_print(args.verbose, 1, f"number_points_line: {number_points_line}")
    v_print(args.verbose, 1,
            f"number_split_adj_read-number_points_line+1-10 :{number_split_adj_read}-{number_points_line}+1-10")
    for loopE in range(start_loopE, number_split_adj_read-number_points_line+1-10 + 1):
        v_print(args.verbose, 1,
                f"add_10_R2={R2[loopE]}+{R2[loopE+1]}+{R2[loopE+2]}+{R2[loopE+3]}+{R2[loopE+4]}+{R2[loopE+5]}+{R2[loopE+6]}+{R2[loopE+7]}+{R2[loopE+8]}+{R2[loopE+9]}")
        add_10_R2 = R2[loopE]+R2[loopE+1]+R2[loopE+2]+R2[loopE+3]+R2[loopE+4] + \
            R2[loopE+5]+R2[loopE+6]+R2[loopE+7]+R2[loopE+8]+R2[loopE+9]
        v_print(args.verbose, 1,
                f"{add_10_R2}>{max_R2}*4 and {SLOPE_log_eval[loopE]}<{max_neg_SLOPE}*0.4)")
        if (add_10_R2 > max_R2*4 and SLOPE_log_eval[loopE] < max_neg_SLOPE*0.4):
            start_power_curve = loopE
            break

    #
    if (start_power_curve+5 >= number_split_adj_read-number_points_line+1-10):
        start_power_curve = number_split_adj_read-number_points_line-15
    #
    v_print(args.verbose, 1,
            f"First segment of power curve:           {start_power_curve}")
    if (start_power_curve == 0):
        v_print(args.verbose, 1,
                f"Cannot find start of power curve, Curve not as expected, Exiting\n")
        # exit()

    if (start_power_curve != start_loopE):
        v_print(args.verbose, 1,
                f"WARNING: First segment should have been -{start_loopE}-. Curve does not start as expected. Data quality may or may not be OK.")

    #
    # end position of power curve
    #
    end_power_curve = number_split_adj_read-5
    #
    v_print(args.verbose, 1,
            f"End position of power curve:             {end_power_curve}")
    v_print(args.verbose, 1,
            f"Adjbin baseline SD at end position:      {adjbin_baseline_SD_split_adj_read[end_power_curve]}")
    v_print(args.verbose, 1,
            f"Adjbin baseline SD at last position:     {adjbin_baseline_SD_split_adj_read[number_split_adj_read-1]}\n")
    #
    #
    # calculate power curve for all points used
    #
    SX = 0
    SSX = 0
    SY = 0
    SSY = 0
    SXY = 0
    n = 0
    #
    v_print(args.verbose, 1,
            f"start_power_curve: {start_power_curve}, end_power_curve: {end_power_curve}, logx {logx}")
    for loopD in range(start_power_curve, end_power_curve):
        loop_now = loopD
        SX = SX+logx[loop_now]
        SSX = SSX+(logx[loop_now]*logx[loop_now])
        SY = SY+logy[loop_now]
        SSY = SSY+(logy[loop_now]*logy[loop_now])
        SXY = SXY+(logx[loop_now]*logy[loop_now])
        n += 1

    #
    SXSY = SX*SY
    SXMYM = SXY-(SXSY/n)
    SXMXM = SSX-(SX*SX/n)
    SXM2 = SXMXM
    SYMYM = SSY-(SY*SY/n)
    SLOPE = SXMYM/SXMXM
    INTER = (SY-SX*SLOPE)/n
    SSESLOPE = SYMYM-SLOPE*SLOPE*SXMXM
    SDSLOPE = math.sqrt(SSESLOPE/(n-2)/SXMXM)
    TTEST = SLOPE/SDSLOPE
    DF = n-2
    v_print(args.verbose, 1,
            f"Power curve stats: (adjust reads start position: {start_power_curve}  end position: {end_power_curve})")
    v_print(args.verbose, 1, f"SLOPE: {SLOPE}  ")
    v_print(args.verbose, 1, f"t TEST: {TTEST}  ")
    v_print(args.verbose, 1, f"NUMBER OF SAMPLES: {n}  ")
    # calculate R2
    SST = SSY
    SS_Regression = SLOPE*SXMYM
    SS_Residual = SST-SS_Regression-SY*SY/n
    SSM = SY*SY/n
    R2_pow = SS_Regression/(SS_Residual+SS_Regression)
    SLOPE_pow = SLOPE
    INTER_pow = INTER
    v_print(args.verbose, 1, f"R2 (FRACTION OF VAR DUE TO REGR): {R2_pow}\n")
    #
    # calculate linear curve for remaining points
    #
    SX = 0.0
    SSX = 0.0
    SY = 0.0
    SSY = 0.0
    SXY = 0.0
    n = 0.0
    #
    for loopD in range(end_power_curve+1, number_split_adj_read+1):
        loop_now = loopD
        SX = SX+linx[loop_now]
        SSX = SSX+(linx[loop_now]*linx[loop_now])
        SY = SY+liny[loop_now]
        SSY = SSY+(liny[loop_now]*liny[loop_now])
        SXY = SXY+(linx[loop_now]*liny[loop_now])
        n += 1

    #
    SXSY = SX*SY
    SXMYM = SXY-(SXSY/n)
    SXMXM = SSX-(SX*SX/n)
    SXM2 = SXMXM
    SYMYM = SSY-(SY*SY/n)
    SLOPE = SXMYM/SXMXM
    INTER = (SY-SX*SLOPE)/n
    SSESLOPE = SYMYM-SLOPE*SLOPE*SXMXM
    SDSLOPE = math.sqrt(SSESLOPE/(n-2)/SXMXM)
    TTEST = SLOPE/SDSLOPE
    DF = n-2
    v_print(args.verbose, 1,
            f"Linear curve stats: (adjust reads start position: {end_power_curve}+1  end position: {number_split_adj_read})")
    v_print(args.verbose, 1, f"SLOPE: {SLOPE} ")
    v_print(args.verbose, 1, f"t TEST: {TTEST}  ")
    v_print(args.verbose, 1, f"NUMBER OF SAMPLES: {n}  ")
    # calculate R2
    SST = SSY
    SS_Regression = SLOPE*SXMYM
    SS_Residual = SST-SS_Regression-SY*SY/n
    SSM = SY*SY/n
    R2_lin = SS_Regression/(SS_Residual+SS_Regression)
    v_print(args.verbose, 1, f"R2 (FRACTION OF VAR DUE TO REGR): {R2_lin}\n")
    #
    #
    #
    v_print(args.verbose, 1, f"Validation of curve fitting:")
    v_print(args.verbose, 1,
            f"Bin\tNumber of adjust reads\tAdjbin Baseline SD\tFitted SD")
    for loopE in range(1, number_split_adj_read+1):
        if (adjust_limit[loopE] > adjust_limit[end_power_curve]):
            log_adjust_limit = math.log(adjust_limit[end_power_curve])
        else:
            log_adjust_limit = math.log(adjust_limit[loopE])

        log_adjbin_baseline_SD_split_adj_read = INTER_pow+SLOPE_pow*log_adjust_limit
        calc_adjbin_baseline_SD_split_adj_read = math.exp(
            log_adjbin_baseline_SD_split_adj_read)
        if (loopE < 10 or loopE == (int((loopE)/10))*10):
            v_print(args.verbose, 1,
                    f"{loopE}\t\t{adjust_limit[loopE]}\t\t{adjbin_baseline_SD_split_adj_read[loopE]}\t\t{calc_adjbin_baseline_SD_split_adj_read}")

    v_print(args.verbose, 1, "\n")
    #
    #
    #
    # calculate sigma values for each genomic bin
    #
    #
    adjbin_total_hits = 0.0
    adjbin_mb_total_hits = 0.0
    #
    counts_totalSD_zero = 0.0
    #
    counts_bcf = 0.0   # bcf: below baseline cutoff
    counts_acf = 0.0   # acf: above baseline cutoff
    counts_icf = 0.0   # icf: in baseline cutoff
    counts_mcf = 0.0   # mcf: in baseline mean
    #
    adjbin_total_hits_bcf = 0.0
    adjbin_total_hits_icf = 0.0
    adjbin_total_hits_mcf = 0.0
    adjbin_total_hits_acf = 0.0
    adjbin_mb_total_hits_bcf = 0.0
    adjbin_mb_total_hits_icf = 0.0
    adjbin_mb_total_hits_mcf = 0.0
    adjbin_mb_total_hits_acf = 0.0
    #
    sigma_bin_totalSD_total_hits_bcf = 0.0
    sigma_bin_totalSD_total_hits_icf = 0.0
    sigma_bin_totalSD_total_hits_mcf = 0.0
    sigma_bin_totalSD_total_hits_acf = 0.0
    sigma_bin_mb_totalSD_total_hits_bcf = 0.0
    sigma_bin_mb_totalSD_total_hits_icf = 0.0
    sigma_bin_mb_totalSD_total_hits_mcf = 0.0
    sigma_bin_mb_totalSD_total_hits_acf = 0.0
    #
    #
    #
    #
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            if (adjust.iat[loopC, loopA] != 0):
                #
                adjbin_mb.iat[loopC, loopA] = adjbin.iat[loopC,
                                                         loopA]-adjbin_baseline_mean
                adjbin_total_hits = adjbin_total_hits+adjbin.iat[loopC, loopA]
                adjbin_mb_total_hits = adjbin_mb_total_hits + \
                    adjbin_mb.iat[loopC, loopA]
                #
                # v_print(args.verbose, 1,f"{adjust.iat[loopC,loopA]}")
                if (adjust.iat[loopC, loopA] > adjust_limit[end_power_curve]):
                    log_adjust_limit = math.log(adjust_limit[end_power_curve])
                else:
                    log_adjust_limit = math.log(adjust.iat[loopC, loopA])

                log_adjbin_baseline_SD_split_adj_read = INTER_pow+SLOPE_pow*log_adjust_limit
                adjbin_SD.iat[loopC, loopA] = math.exp(
                    log_adjbin_baseline_SD_split_adj_read)
                #
                sigma_bin_totalSD.iat[loopC, loopA] = adjbin.iat[loopC,
                                                                 loopA]/adjbin_SD.iat[loopC, loopA]
                sigma_bin_mb_totalSD.iat[loopC, loopA] = adjbin_mb.iat[loopC,
                                                                       loopA]/adjbin_SD.iat[loopC, loopA]  # variable to USE
                #
                # calculate various total values for bins above and below and near baseline cutoff
                #
                if (adjbin.iat[loopC, loopA] <= adjbin_baseline_cutoff):
                    counts_bcf = counts_bcf+1
                    #
                    adjbin_total_hits_bcf = adjbin_total_hits_bcf + \
                        adjbin.iat[loopC, loopA]
                    adjbin_mb_total_hits_bcf = adjbin_mb_total_hits_bcf + \
                        adjbin_mb.iat[loopC, loopA]
                    #
                    sigma_bin_totalSD_total_hits_bcf = sigma_bin_totalSD_total_hits_bcf + \
                        sigma_bin_totalSD.iat[loopC, loopA]
                    sigma_bin_mb_totalSD_total_hits_bcf = sigma_bin_mb_totalSD_total_hits_bcf + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA]
                    #
                else:
                    counts_acf = counts_acf+1
                    #
                    adjbin_total_hits_acf = adjbin_total_hits_acf + \
                        adjbin.iat[loopC, loopA]
                    adjbin_mb_total_hits_acf = adjbin_mb_total_hits_acf + \
                        adjbin_mb.iat[loopC, loopA]
                    #
                    sigma_bin_totalSD_total_hits_acf = sigma_bin_totalSD_total_hits_acf + \
                        sigma_bin_totalSD.iat[loopC, loopA]
                    sigma_bin_mb_totalSD_total_hits_acf = sigma_bin_mb_totalSD_total_hits_acf + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA]
                    #
                #
                if (adjbin.iat[loopC, loopA] > adjbin_baseline_cutoff*0.95 and adjbin.iat[loopC, loopA] < adjbin_baseline_cutoff*1.05):
                    counts_icf = counts_icf+1
                    #
                    adjbin_total_hits_icf = adjbin_total_hits_icf + \
                        adjbin.iat[loopC, loopA]
                    adjbin_mb_total_hits_icf = adjbin_mb_total_hits_icf + \
                        adjbin_mb.iat[loopC, loopA]
                    #
                    sigma_bin_totalSD_total_hits_icf = sigma_bin_totalSD_total_hits_icf + \
                        sigma_bin_totalSD.iat[loopC, loopA]
                    sigma_bin_mb_totalSD_total_hits_icf = sigma_bin_mb_totalSD_total_hits_icf + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA]
                    #
                #
                if (adjbin.iat[loopC, loopA] > adjbin_baseline_mean*0.95 and adjbin.iat[loopC, loopA] < adjbin_baseline_mean*1.05):
                    counts_mcf = counts_mcf+1
                    #
                    adjbin_total_hits_mcf = adjbin_total_hits_mcf + \
                        adjbin.iat[loopC, loopA]
                    adjbin_mb_total_hits_mcf = adjbin_mb_total_hits_mcf + \
                        adjbin_mb.iat[loopC, loopA]
                    #
                    sigma_bin_totalSD_total_hits_mcf = sigma_bin_totalSD_total_hits_mcf + \
                        sigma_bin_totalSD.iat[loopC, loopA]
                    sigma_bin_mb_totalSD_total_hits_mcf = sigma_bin_mb_totalSD_total_hits_mcf + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA]
                    #

                #

    #
    adjbin_total_hits = int(adjbin_total_hits+0.5)
    adjbin_mb_total_hits = int(adjbin_mb_total_hits+0.5)
    #
    v_print(args.verbose, 1, f"Step 3: Calculate sigma values at each genomic bin:\n")
    v_print(args.verbose, 1,
            f"Total recalculated sample hits:  {bin_recalc_total_hits}")
    v_print(args.verbose, 1,
            f"Total recalculated adjust hits:  {adjust_recalc_total_hits}")
    v_print(args.verbose, 1,
            f"Correction factor from bin to adjbin: {Acorr_factor}\n")
    #
    v_print(args.verbose, 1,
            f"Total adjbin sample hits (adjbin):                                         {adjbin_total_hits}")
    v_print(args.verbose, 1,
            f"Total adjbin sample minus mean baseline hits (adjbin_mb):                  {adjbin_mb_total_hits}")
    v_print(args.verbose, 1,
            f"Number of bins with totalSD equal to zero: {counts_totalSD_zero}\n")

    adjbin_mean_hits_bcf = adjbin_total_hits_bcf/counts_bcf
    adjbin_mean_hits_acf = adjbin_total_hits_acf/counts_acf
    adjbin_mean_hits_icf = adjbin_total_hits_icf/counts_icf
    adjbin_mean_hits_mcf = adjbin_total_hits_mcf/counts_mcf
    #
    adjbin_mb_mean_hits_bcf = adjbin_mb_total_hits_bcf/counts_bcf
    adjbin_mb_mean_hits_acf = adjbin_mb_total_hits_acf/counts_acf
    adjbin_mb_mean_hits_icf = adjbin_mb_total_hits_icf/counts_icf
    adjbin_mb_mean_hits_mcf = adjbin_mb_total_hits_mcf/counts_mcf
    #
    sigma_bin_totalSD_mean_hits_bcf = sigma_bin_totalSD_total_hits_bcf/counts_bcf
    sigma_bin_totalSD_mean_hits_acf = sigma_bin_totalSD_total_hits_acf/counts_acf
    sigma_bin_totalSD_mean_hits_icf = sigma_bin_totalSD_total_hits_icf/counts_icf
    sigma_bin_totalSD_mean_hits_mcf = sigma_bin_totalSD_total_hits_mcf/counts_mcf
    #
    sigma_bin_mb_totalSD_mean_hits_bcf = sigma_bin_mb_totalSD_total_hits_bcf/counts_bcf
    sigma_bin_mb_totalSD_mean_hits_acf = sigma_bin_mb_totalSD_total_hits_acf/counts_acf
    sigma_bin_mb_totalSD_mean_hits_icf = sigma_bin_mb_totalSD_total_hits_icf/counts_icf
    sigma_bin_mb_totalSD_mean_hits_mcf = sigma_bin_mb_totalSD_total_hits_mcf/counts_mcf
    #
    total_bins = counts_bcf+counts_acf
    percent_counts_bcf = int(counts_bcf/total_bins*100)
    percent_counts_acf = int(counts_acf/total_bins*100)
    #
    v_print(args.verbose, 1,
            f"Number of bins at or below baseline cutoff: {counts_bcf}     Percent: {percent_counts_bcf}")
    v_print(args.verbose, 1,
            f"Number of bins above baseline cutoff:       {counts_acf}     Percent: {percent_counts_acf}")
    v_print(args.verbose, 1,
            f"Total bins (with adjust non-zero):          {total_bins}\n")
    #
    v_print(args.verbose, 1, f"Adjbin values")
    v_print(args.verbose, 1,
            f"Mean adjbin value for bins below baseline cutoff:       {adjbin_mean_hits_bcf}")
    v_print(args.verbose, 1,
            f"Mean adjbin value for bins at baseline mean:            {adjbin_mean_hits_mcf}")
    v_print(args.verbose, 1,
            f"Mean adjbin value for bins at baseline cutoff:          {adjbin_mean_hits_icf}")
    v_print(args.verbose, 1,
            f"Mean adjbin value for bins above baseline cutoff:       {adjbin_mean_hits_acf}\n")
    #
    v_print(args.verbose, 1,
            f"Mean adjbin_mb value for bins below baseline cutoff:    {adjbin_mb_mean_hits_bcf}")
    v_print(args.verbose, 1,
            f"Mean adjbin_mb value for bins at baseline mean:         {adjbin_mb_mean_hits_mcf}")
    v_print(args.verbose, 1,
            f"Mean adjbin_mb value for bins at baseline cutoff:       {adjbin_mb_mean_hits_icf}")
    v_print(args.verbose, 1,
            f"Mean adjbin_mb value for bins above baseline cutoff:    {adjbin_mb_mean_hits_acf}\n")
    #
    v_print(args.verbose, 1, f"Sigma values")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin/total SD) for bins below baseline cutoff:      {sigma_bin_totalSD_mean_hits_bcf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin/total SD) for bins at baseline mean:           {sigma_bin_totalSD_mean_hits_mcf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin/total SD) for bins at baseline cutoff:         {sigma_bin_totalSD_mean_hits_icf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin/total SD) for bins above baseline cutoff:      {sigma_bin_totalSD_mean_hits_acf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin_mb/total SD) for bins below baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_bcf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin_mb/total SD) for bins at baseline mean:        {sigma_bin_mb_totalSD_mean_hits_mcf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin_mb/total SD) for bins at baseline cutoff:      {sigma_bin_mb_totalSD_mean_hits_icf}")
    v_print(args.verbose, 1,
            f"Mean sigma (adjbin_mb/total SD) for bins above baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_acf}\n")
    #
    v_print(args.verbose, 1,
            f"\nStep 4: Process sigma values: interpolate, smoothen, trim, convert to log2:\n")
    # interpolate data when one empty bin between two full bins
    #    interpolated bins get values:   $adjust[$loopC][$loopA]=-1
    counts_interpolation = 0
    for loopC in range(1, 23+1):
        for loopA in range(2, int(chr_size[loopC-1])):
            if (adjust.iat[loopC, loopA-2] > 0 and adjust.iat[loopC, loopA-1] == 0 and adjust.iat[loopC, loopA] > 0):
                #
                weight_pre = adjust.iat[loopC, loopA-2] / \
                    (adjust.iat[loopC, loopA-2] + adjust.iat[loopC, loopA])
                weight_post = adjust.iat[loopC, loopA] / \
                    (adjust.iat[loopC, loopA-2] + adjust.iat[loopC, loopA])
                #
                adjbin.iat[loopC, loopA-1] = adjbin.iat[loopC, loopA-2] * \
                    weight_pre + adjbin.iat[loopC, loopA]*weight_post
                adjbin_mb.iat[loopC, loopA-1] = adjbin_mb.iat[loopC, loopA-2] * \
                    weight_pre + adjbin_mb.iat[loopC, loopA]*weight_post
                #
                sigma_bin_totalSD.iat[loopC, loopA-1] = sigma_bin_totalSD.iat[loopC,
                                                                              loopA-2]*weight_pre + sigma_bin_totalSD.iat[loopC, loopA]*weight_post
                sigma_bin_mb_totalSD.iat[loopC, loopA-1] = sigma_bin_mb_totalSD.iat[loopC,
                                                                                    loopA-2]*weight_pre + sigma_bin_mb_totalSD.iat[loopC, loopA]*weight_post
                #
                adjust.iat[loopC, loopA-1] = -1
                counts_interpolation = counts_interpolation+1

    v_print(args.verbose, 1,
            f"Number of bins interpolated:            {counts_interpolation}")
    # smoothen adjbin, sigma values
    #
    #   if central value below  adjbin_cutoff  and either of the side values is above  adjbin_cutoff  do not smoothen
    #
    counts_sm = 0
    for loopC in range(1, 23+1):
        for loopA in range(1, int(chr_size[loopC-1])):
            #
            sigma_bin_totalSD_sm.iat[loopC,
                                     loopA] = sigma_bin_totalSD.iat[loopC, loopA]
            sigma_bin_mb_totalSD_sm.iat[loopC,
                                        loopA] = sigma_bin_mb_totalSD.iat[loopC, loopA]
            #
            if (adjust.iat[loopC, loopA-1] != 0 and adjust.iat[loopC, loopA] != 0 and adjust.iat[loopC, loopA+1] != 0):
                if ((adjbin.iat[loopC, loopA-1] > adjbin_baseline_cutoff or adjbin.iat[loopC, loopA+1] > adjbin_baseline_cutoff) and adjbin.iat[loopC, loopA] <= adjbin_baseline_cutoff):
                    donothing = 0
                else:
                    #
                    weight_pre = adjust.iat[loopC, loopA-1] / (
                        adjust.iat[loopC, loopA-1] + adjust.iat[loopC, loopA]*4 + adjust.iat[loopC, loopA+1])
                    weight_now = adjust.iat[loopC, loopA]*4 / (
                        adjust.iat[loopC, loopA-1] + adjust.iat[loopC, loopA]*4 + adjust.iat[loopC, loopA+1])
                    weight_post = adjust.iat[loopC, loopA+1] / (
                        adjust.iat[loopC, loopA-1] + adjust.iat[loopC, loopA]*4 + adjust.iat[loopC, loopA+1])
                    #
                    sigma_bin_totalSD_sm.iat[loopC, loopA] = sigma_bin_totalSD.iat[loopC, loopA-1]*weight_pre + \
                        sigma_bin_totalSD.iat[loopC, loopA]*weight_now + \
                        sigma_bin_totalSD.iat[loopC, loopA+1]*weight_post
                    sigma_bin_mb_totalSD_sm.iat[loopC, loopA] = sigma_bin_mb_totalSD.iat[loopC, loopA-1]*weight_pre + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA]*weight_now + \
                        sigma_bin_mb_totalSD.iat[loopC, loopA+1]*weight_post
                    #
                    counts_sm = counts_sm+1
                    #
    #
    v_print(args.verbose, 1,
            f"Number of sigma bins smoothened:        {counts_sm}")
    # sigma_bin_totalSD    sigma_bin_mb_totalSD
    #
    counts_tm = 0
    for loopC in range(1, 23+1):
        for loopA in range(1, int(chr_size[loopC-1])):
            sigma_bin_totalSD_tm.iat[loopC,
                                     loopA] = sigma_bin_totalSD.iat[loopC, loopA]
            sigma_bin_mb_totalSD_tm.iat[loopC,
                                        loopA] = sigma_bin_mb_totalSD.iat[loopC, loopA]
            if (adjust.iat[loopC, loopA-1] != 0 and adjust.iat[loopC, loopA] != 0 and adjust.iat[loopC, loopA+1] != 0):
                #
                if (sigma_bin_mb_totalSD.iat[loopC, loopA] > sigma_bin_mb_totalSD.iat[loopC, loopA-1]*1.2 and sigma_bin_mb_totalSD.iat[loopC, loopA] > sigma_bin_mb_totalSD.iat[loopC, loopA+1]*1.2):
                    #
                    if (sigma_bin_totalSD.iat[loopC, loopA-1] >= sigma_bin_totalSD.iat[loopC, loopA+1]):
                        sigma_bin_totalSD_tm.iat[loopC,
                                                 loopA] = sigma_bin_totalSD.iat[loopC, loopA-1]*1.2
                    else:
                        sigma_bin_totalSD_tm.iat[loopC,
                                                 loopA] = sigma_bin_totalSD.iat[loopC, loopA+1]*1.2

                    #
                    if (sigma_bin_mb_totalSD.iat[loopC, loopA-1] >= sigma_bin_mb_totalSD.iat[loopC, loopA+1]):
                        sigma_bin_mb_totalSD_tm.iat[loopC,
                                                    loopA] = sigma_bin_mb_totalSD.iat[loopC, loopA-1]*1.2
                    else:
                        sigma_bin_mb_totalSD_tm.iat[loopC,
                                                    loopA] = sigma_bin_mb_totalSD.iat[loopC, loopA+1]*1.2

                    #
                    counts_tm = counts_tm+1

                #
    v_print(args.verbose, 1,
            f"Number of sigma bins trimmed:           {counts_tm}\n")
    #
    # convert  sigma_bin_mb  to log2 (non-smoothened, smoothened, trimmed)
    #   after log conversion want log2 of  sigma_bin_mb_totalSD_mean_hits_mcf  to be equal to zero (as it is before log2 conversion)
    #      hence need to add a constant value to all   sigma_bin_mb_totalSD   values
    #
    adj_to_log2 = 1-sigma_bin_mb_totalSD_mean_hits_mcf
    #
    if (sigma_bin_mb_totalSD_mean_hits_icf+adj_to_log2 > 1):
        sigma_bin_mb_totalSD_mean_hits_icf_log2 = math.log(
            sigma_bin_mb_totalSD_mean_hits_icf+adj_to_log2)/math.log(2)
    else:
        sigma_bin_mb_totalSD_mean_hits_icf_log2 = 0

    #
    if (sigma_bin_mb_totalSD_mean_hits_acf+adj_to_log2 > 1):
        sigma_bin_mb_totalSD_mean_hits_acf_log2 = math.log(
            sigma_bin_mb_totalSD_mean_hits_acf+adj_to_log2)/math.log(2)
    else:
        sigma_bin_mb_totalSD_mean_hits_acf_log2 = 0

    #
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            if (adjust.iat[loopC, loopA] != 0):
                #
                if (sigma_bin_mb_totalSD.iat[loopC, loopA]+adj_to_log2 > 1):
                    sigma_bin_mb_totalSD_log2.iat[loopC, loopA] = math.log(
                        sigma_bin_mb_totalSD.iat[loopC, loopA]+adj_to_log2)/math.log(2)
                if (sigma_bin_mb_totalSD_sm.iat[loopC, loopA]+adj_to_log2 > 1):
                    sigma_bin_mb_totalSD_sm_log2.iat[loopC, loopA] = math.log(
                        sigma_bin_mb_totalSD_sm.iat[loopC, loopA]+adj_to_log2)/math.log(2)
                if (sigma_bin_mb_totalSD_tm.iat[loopC, loopA]+adj_to_log2 > 1):
                    sigma_bin_mb_totalSD_tm_log2.iat[loopC, loopA] = math.log(
                        sigma_bin_mb_totalSD_tm.iat[loopC, loopA]+adj_to_log2)/math.log(2)
                #
    #
    # find maximum values for  tm_log2  scale
    #
    max_sigma_bin_mb_totalSD_tm_log2 = list()
    #
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            if (adjust.iat[loopC, loopA] != 0):
                #
                max_sigma_bin_mb_totalSD_tm_log2.append(
                    sigma_bin_mb_totalSD_tm_log2.iat[loopC, loopA])
                #

    # we want to return the sorted list not sort in place (.sort())
    sort_max_sigma_bin_mb_totalSD_tm_log2 = sorted(
        max_sigma_bin_mb_totalSD_tm_log2)
    #
    n = len(sort_max_sigma_bin_mb_totalSD_tm_log2)-1
    sigma_bin_mb_totalSD_tm_log2_max_hit = int(
        sort_max_sigma_bin_mb_totalSD_tm_log2[n]*1000+0.5)/1000
    sigma_bin_mb_totalSD_tm_log2_top10_hits = int(
        (sort_max_sigma_bin_mb_totalSD_tm_log2[n-5]+sort_max_sigma_bin_mb_totalSD_tm_log2[n-10]+sort_max_sigma_bin_mb_totalSD_tm_log2[n-15]+sort_max_sigma_bin_mb_totalSD_tm_log2[n-20])*250+0.5)/1000
    #
    #
    #
    # calculate t6 variables to leave only top6 log2 values from  smoothened adjbin_log2 peaks
    #
    # for (loopC=1 loopC<=23 loopC++){
    #    for (loopA=0 loopA<chr_size[loopC] loopA++) {
    #        if (adjust.iat[loopC,loopA]!=0) {
    #            #
    #            adjbin_smt6_log2.iat[loopC,loopA]=adjbin_sm_log2[loopC,loopA]-adjbin_sm_top10_hits_log2+6
    #            if (adjbin_smt6_log2.iat[loopC,loopA]<0) {adjbin_smt6_log2.iat[loopC,loopA]=0}
    #            #
    #        }
    #    }
    # }
    #
    v_print(args.verbose, 1, f"Log2 conversions")
    v_print(args.verbose, 1,
            f"Log2 of mean sigma (bin_mb/total SD) for bins at baseline mean:        set to 0")
    v_print(args.verbose, 1,
            f"Log2 of mean sigma (bin_mb/total SD) for bins at baseline cutoff:      {sigma_bin_mb_totalSD_mean_hits_icf_log2}")
    v_print(args.verbose, 1,
            f"Log2 of mean sigma (bin_mb/total SD) for bins above baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_acf_log2}")
    v_print(args.verbose, 1,
            f"Log2 of max sigma value (tm):                                          {sigma_bin_mb_totalSD_tm_log2_max_hit}")
    v_print(args.verbose, 1,
            f"Log2 of top 10 sigma values (tm):                                      {sigma_bin_mb_totalSD_tm_log2_top10_hits}\n")
    #
    # limit decimals
    #
    for loopC in range(1, 23+1):
        for loopA in range(0, int(chr_size[loopC-1])):
            #
            adjbin_SD.iat[loopC, loopA] = int(
                adjbin_SD.iat[loopC, loopA]*1000+0.5)/1000
            #
            adjbin.iat[loopC, loopA] = int(
                adjbin.iat[loopC, loopA]*1000+0.5)/1000
            adjbin_mb.iat[loopC, loopA] = int(
                adjbin_mb.iat[loopC, loopA]*1000+0.5)/1000
            #
            sigma_bin_totalSD.iat[loopC, loopA] = int(
                sigma_bin_totalSD.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_totalSD_sm.iat[loopC, loopA] = int(
                sigma_bin_totalSD_sm.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_totalSD_tm.iat[loopC, loopA] = int(
                sigma_bin_totalSD_tm.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_mb_totalSD.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_mb_totalSD_sm.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD_sm.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_mb_totalSD_tm.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD_tm.iat[loopC, loopA]*1000+0.5)/1000
            #
            sigma_bin_mb_totalSD_log2.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD_log2.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_mb_totalSD_sm_log2.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD_sm_log2.iat[loopC, loopA]*1000+0.5)/1000
            sigma_bin_mb_totalSD_tm_log2.iat[loopC, loopA] = int(
                sigma_bin_mb_totalSD_tm_log2.iat[loopC, loopA]*1000+0.5)/1000
            #
    #
    # write files
    #

    # adjbin_hits.to_csv("adjbin_hits.csv")
    # sam_repl_file="SRR6365046_chr1_T2T_sorted_1.bam"
    # result_file = f"{sam_repl_file}_bin-size_{bin_size}_quality_{quality_score}_chr1-X_adjbin_qual_counts_0b.txt"
    #
    # with open(result_file, 'w') as RESULT:
    #
    #    RESULT.write("\n")
    #    RESULT.write(f"file {sam_repl_file}\n");
    #    RESULT.write(f"quality OK {qualOK}\n");
    #    RESULT.write(f"quality not OK {qualnotOK}\n\n");
    #    RESULT.write(f"\n");
    #    RESULT.write(f"Total sample hits: {bin_total_hits}\n");
    #    RESULT.write(f"Adjust total hits: {adjust_total_hits}\n");
    #    RESULT.write(f"Correction factor: {Acorr_factor}\n");
    #    RESULT.write(f"Total adjusted sample hits: {adjbin_total_hits}\n\n");
    #
    # result_file = f"{sam_repl_file}_bin-size_{bin_size}_quality_{quality_score}_chr1-X_adjbin_0b.csv"
    # v_print(args.verbose, 1,number_bins)

    # with open(result_file, 'w') as RESULT:
    #   for loopb in range(0,number_bins+1):
    #       for loopc in range(1,23+1):
    #           RESULT.write(f"{bin_hits.iat[loopc,loopb]},{adjbin_hits.iat[loopc,loopb]},{adjust_hits.iat[loopc,loopb]},,")
    #       RESULT.write("\n")

    v_print(args.verbose, 1, f"Step 5: Write sigma files and long files:\n")

    v_print(args.verbose, 1, f"- {sam_repl_file}_sigma_all_0b.csv\n")
    result_file = f"{sam_repl_file}_sigma_all_0b.csv"
    #
    # v_print(args.verbose, 1,f"{sigma_bin_totalSD_sm}")
    with open(result_file, 'w') as RESULT:
        for loopA in range(0, max_number_bins):
            for loopC in range(1, 23+1):
                # variables 0-1
                RESULT.write(
                    f"{sigma_bin_mb_totalSD_sm.iat[loopC,loopA]},{sigma_bin_mb_totalSD_tm.iat[loopC,loopA]},")
                # variables 2-4
                RESULT.write(
                    f"{sigma_bin_totalSD_sm.iat[loopC,loopA]},{sigma_bin_totalSD_tm.iat[loopC,loopA]},{adjust.iat[loopC,loopA]},")
                # variables 5-6
                RESULT.write(
                    f"{sigma_bin_mb_totalSD.iat[loopC,loopA]},{sigma_bin_totalSD.iat[loopC,loopA]},")
                # variables 7-9
                RESULT.write(
                    f"{sigma_bin_mb_totalSD_log2.iat[loopC,loopA]},{sigma_bin_mb_totalSD_sm_log2.iat[loopC,loopA]},{sigma_bin_mb_totalSD_tm_log2.iat[loopC,loopA]},")
                # variables 10-11
                RESULT.write(
                    f"{adjbin.iat[loopC,loopA]},{adjbin_mb.iat[loopC,loopA]},")
                # variable 12-13
                RESULT.write(
                    f"{adjbin_SD.iat[loopC,loopA]},{bin.iat[loopC,loopA]},,")
            RESULT.write(f"\n")

    result_file = f"{sam_repl_file}_sigma_all_0b_long.csv"
    v_print(args.verbose, 1, f"- {sam_repl_file}_sigma_all_0b_long.csv\n")
    #
    # v_print(args.verbose, 1,f"{sigma_bin_totalSD_sm}")
    with open(result_file, 'w') as RESULT:
        for loopA in range(0, max_number_bins):
            for loopC in range(1, 23+1):
                # variables 0-1
                RESULT.write(
                    f"{loopA},{loopC},{sigma_bin_mb_totalSD_sm.iat[loopC,loopA]},{sigma_bin_mb_totalSD_tm.iat[loopC,loopA]},")
                # variables 2-4
                RESULT.write(
                    f"{sigma_bin_totalSD_sm.iat[loopC,loopA]},{sigma_bin_totalSD_tm.iat[loopC,loopA]},{adjust.iat[loopC,loopA]},")
                # variables 5-6
                RESULT.write(
                    f"{sigma_bin_mb_totalSD.iat[loopC,loopA]},{sigma_bin_totalSD.iat[loopC,loopA]},")
                # variables 7-9
                RESULT.write(
                    f"{sigma_bin_mb_totalSD_log2.iat[loopC,loopA]},{sigma_bin_mb_totalSD_sm_log2.iat[loopC,loopA]},{sigma_bin_mb_totalSD_tm_log2.iat[loopC,loopA]},")
                # variables 10-11
                RESULT.write(
                    f"{adjbin.iat[loopC,loopA]},{adjbin_mb.iat[loopC,loopA]},")
                # variable 12-13
                RESULT.write(
                    f"{adjbin_SD.iat[loopC,loopA]},{bin.iat[loopC,loopA]}\n")
            RESULT.write(f"")

    # fast hack to sort the output, read it in again and overwrite
    v_print(args.verbose, 1,
            f"- {sam_repl_file}_sigma_all_0b_long_sorted.csv\n")
    df = pd.read_csv(result_file, delimiter=",", header=None, names=["bin_index", "chr_index", "sigma_bin_mb_totalSD_sm", "sigma_bin_mb_totalSD_tm", "sigma_bin_totalSD_sm", "sigma_bin_totalSD_tm",
                     "adjust", "sigma_bin_mb_totalSD", "sigma_bin_totalSD", "sigma_bin_mb_totalSD_log2", "sigma_bin_mb_totalSD_sm_log2", "sigma_bin_mb_totalSD_tm_log2", "adjbin", "adjbin_mb", "adjbin_SD", "bin"])
    # parameter ascending is applied to 'col1' and 'col2' respectively.
    df = df.sort_values(['chr_index', 'bin_index'], ascending=[True, True])
    df.to_csv(f"{sam_repl_file}_sigma_all_0b_long_sorted.csv", index=False)
    #
    #
    #
    #
    # sam_repl_file = "SRR6365046_chr1_T2T"
    v_print(args.verbose, 1,
            f"- {sam_repl_file}_sigma_select_0b.csv\n")
    result_file = f"{sam_repl_file}_sigma_select_0b.csv"
    #
    with open(result_file, 'w') as RESULT:
        for loopA in range(0, max_number_bins):
            for loopC in range(1, 23+1):
                # variables 0-1
                RESULT.write(
                    f"{sigma_bin_mb_totalSD_sm.iat[loopC,loopA]},{sigma_bin_mb_totalSD_tm.iat[loopC,loopA]},")
                # variables 2-4
                RESULT.write(
                    f"{sigma_bin_totalSD_sm.iat[loopC,loopA]},{sigma_bin_totalSD_tm.iat[loopC,loopA]},{adjust.iat[loopC,loopA]},,")
            RESULT.write(f"\n")
    #
    # last statistics text file
    #
    result_file = f"{sam_repl_file}_sigma_qual_counts_0b.txt"
    v_print(args.verbose, 1, f"- {sam_repl_file}_sigma_qual_counts_0b.txt")
    # print "$result_File\n";
    #
    with open(result_file, 'w') as RESULT:
        #
        RESULT.write(f"File Name: {file_Name}\n")
        RESULT.write(f"Bin size: {bin_size}\n")
        RESULT.write(f"Max number of bins: {max_number_bins}\n")
        RESULT.write(f"Total sample hits: {bin_total_hits}\n")
        RESULT.write(f"Total adjust hits: {adjust_total_hits}\n")
        RESULT.write(
            f"Correction factor from bin to adjbin: {Acorr_factor}\n\n")
        #
        RESULT.write(
            f"Percentile at low baseline cutoff: {percentile_baseline_cutoff_low}\n")
        RESULT.write(
            f"Baseline of adjbin data values at low cutoff: {adjbin_baseline_cutoff_low}\n\n")
        #
        RESULT.write(
            f"Percentile at high baseline cutoff: {percentile_baseline_cutoff_high}\n")
        RESULT.write(
            f"Baseline of adjbin data values at high cutoff: {adjbin_baseline_cutoff_high}\n\n")
        #
        RESULT.write(
            f"Baseline of adjbin data values (selected baseline cutoff): {adjbin_baseline_cutoff}\n")
        RESULT.write(f"Percentile at baseline: {percentile_baseline_cutoff}\n")
        RESULT.write(
            f"Percentile used to calculate SD of baseline\n (should be equal to percentile at baseline): {used_percentile_baseline}\n")
        RESULT.write(
            f"Mean baseline of adjbin data values: {adjbin_baseline_mean}\n")
        RESULT.write(
            f"ANOVA-based SD of mean baseline of adjbin data values: {adjbin_baseline_SD}\n")
        RESULT.write(
            f"Number of adjbin data values used to calculate mean and SD: {nAdjBin}\n")
        #
        RESULT.write(
            f"Total recalculated sample hits:  {bin_recalc_total_hits}\n")
        RESULT.write(
            f"Total recalculated adjust hits:  {adjust_recalc_total_hits}\n")
        RESULT.write(
            f"Correction factor from bin to adjbin: {Acorr_factor}\n\n")
        #
        RESULT.write(
            f"Total adjbin sample hits (adjbin):                                         {adjbin_total_hits}\n")
        RESULT.write(
            f"Total adjbin sample minus mean baseline hits (adjbin_mb):                  {adjbin_mb_total_hits}\n")
        RESULT.write(
            f"Number of bins with totalSD equal to zero:    {counts_totalSD_zero}\n\n")
        #
        RESULT.write(
            f"Number of bins at or below baseline cutoff: {counts_bcf}    Percent: {percent_counts_bcf}\n")
        RESULT.write(
            f"Number of bins above baseline cutoff:       {counts_acf}    Percent: {percent_counts_acf}\n")
        RESULT.write(
            f"Total bins (with adjust non-zero):          {total_bins}\n\n")
        #
        RESULT.write(f"Adjbin values\n")
        RESULT.write(
            f"Mean adjbin value for bins below baseline cutoff:       {adjbin_mean_hits_bcf}\n")
        RESULT.write(
            f"Mean adjbin value for bins at baseline mean:            {adjbin_mean_hits_mcf}\n")
        RESULT.write(
            f"Mean adjbin value for bins at baseline cutoff:          {adjbin_mean_hits_icf}\n")
        RESULT.write(
            f"Mean adjbin value for bins above baseline cutoff:       {adjbin_mean_hits_acf}\n\n")
        #
        RESULT.write(
            f"Mean adjbin_mb value for bins below baseline cutoff:    {adjbin_mb_mean_hits_bcf}\n")
        RESULT.write(
            f"Mean adjbin_mb value for bins at baseline mean:         {adjbin_mb_mean_hits_mcf}\n")
        RESULT.write(
            f"Mean adjbin_mb value for bins at baseline cutoff:       {adjbin_mb_mean_hits_icf}\n")
        RESULT.write(
            f"Mean adjbin_mb value for bins above baseline cutoff:    {adjbin_mb_mean_hits_acf}\n\n")
        #
        RESULT.write(f"Sigma values\n")
        RESULT.write(
            f"Mean sigma (adjbin/total SD) for bins below baseline cutoff:      {sigma_bin_totalSD_mean_hits_bcf}\n")
        RESULT.write(
            f"Mean sigma (adjbin/total SD) for bins at baseline mean:           {sigma_bin_totalSD_mean_hits_mcf}\n")
        RESULT.write(
            f"Mean sigma (adjbin/total SD) for bins at baseline cutoff:         {sigma_bin_totalSD_mean_hits_icf}\n")
        RESULT.write(
            f"Mean sigma (adjbin/total SD) for bins above baseline cutoff:      {sigma_bin_totalSD_mean_hits_acf}\n")
        RESULT.write(
            f"Mean sigma (adjbin_mb/total SD) for bins below baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_bcf}\n")
        RESULT.write(
            f"Mean sigma (adjbin_mb/total SD) for bins at baseline mean:        {sigma_bin_mb_totalSD_mean_hits_mcf}\n")
        RESULT.write(
            f"Mean sigma (adjbin_mb/total SD) for bins at baseline cutoff:      {sigma_bin_mb_totalSD_mean_hits_icf}\n")
        RESULT.write(
            f"Mean sigma (adjbin_mb/total SD) for bins above baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_acf}\n\n")
        #
        RESULT.write(
            f"Number of bins interpolated:            {counts_interpolation}\n")
        RESULT.write(f"Number of adjbin/sigma bins smoothened: {counts_sm}\n")
        RESULT.write(
            f"Number of sigma bins trimmed:           {counts_tm}\n\n")
        #
        RESULT.write(f"Log2 conversions\n")
        RESULT.write(
            f"Log2 of mean sigma (adjbin_mb/total SD) for bins at baseline mean:        set to 0\n")
        RESULT.write(
            f"Log2 of mean sigma (adjbin_mb/total SD) for bins at baseline cutoff:      {sigma_bin_mb_totalSD_mean_hits_icf_log2}\n")
        RESULT.write(
            f"Log2 of mean sigma (adjbin_mb/total SD) for bins above baseline cutoff:   {sigma_bin_mb_totalSD_mean_hits_acf_log2}\n")
        RESULT.write(
            f"Log2 of max sigma value (tm):                                             {sigma_bin_mb_totalSD_tm_log2_max_hit}\n")
        RESULT.write(
            f"Log2 of top 10 sigma values (tm):                                         {sigma_bin_mb_totalSD_tm_log2_top10_hits}\n\n")
        #

elif args.scripts == '2bw':

    # TODO adapt the genomes to all genomes i implemented for now only GRCH38 is done for bigwig conversion
    # create a dictionary mapping chromosome names to their lengths
    chrom_lengths = {
        "chr1": 248956422,
        "chr2": 242193529,
        "chr3": 198295559,
        "chr4": 190214555,
        "chr5": 181538259,
        "chr6": 170805979,
        "chr7": 159345973,
        "chr8": 145138636,
        "chr9": 138394717,
        "chr10": 133797422,
        "chr11": 135086622,
        "chr12": 133275309,
        "chr13": 114364328,
        "chr14": 107043718,
        "chr15": 101991189,
        "chr16": 90338345,
        "chr17": 83257441,
        "chr18": 80373285,
        "chr19": 58617616,
        "chr20": 64444167,
        "chr21": 46709983,
        "chr22": 50818468,
        "chrX": 156040895
    }

    chrom_mapping = {
        "1": "chr1",
        "2": "chr2",
        "3": "chr3",
        "4": "chr4",
        "5": "chr5",
        "6": "chr6",
        "7": "chr7",
        "8": "chr8",
        "9": "chr9",
        "10": "chr10",
        "11": "chr11",
        "12": "chr12",
        "13": "chr13",
        "14": "chr14",
        "15": "chr15",
        "16": "chr16",
        "17": "chr17",
        "18": "chr18",
        "19": "chr19",
        "20": "chr20",
        "21": "chr21",
        "22": "chr22",
        "23": "chrX"
    }

    v_print(args.verbose, 1, 'Bigwig conversion script is running...')
    v_print(args.verbose, 0, f'Conversion File: {args.csv_file}')

    import pyBigWig
    bin_size = args.bin_size  # Update this value if it's different.
    # Assuming you get this from command line arguments.
    csv_file = args.csv_file

    def generate_bigwig_file(bigwig_file, choosen_value, csv_file, chrom_mapping, bin_size):
        """
        Generate a BigWig file based on a chosen value from the CSV data.

        Args:
        - bigwig_file (str): Path to the output BigWig file.
        - choosen_value (int): The column index from the CSV to be used.
        - csv_file (str): Path to the input CSV file.
        - chrom_mapping (dict): Mapping of chromosome names.
        - bin_size (int): Size of the bins.

        Returns:
        None. Writes to the specified BigWig file.
        """
        print(f"Generating BigWig file: {bigwig_file}")

        # Read the CSV file
        with open(csv_file, "r") as file:
            lines = file.readlines()

        updated_lines = []
        for i, line in enumerate(lines):
            # Skip the header
            if i == 0:
                continue
            cols = line.strip().split(",")
            chrom_name = chrom_mapping.get(cols[1], "NA")
            updated_line = ",".join([cols[0], chrom_name, cols[choosen_value]])
            updated_lines.append(updated_line + "\n")  # append newline here

        # Create a new pyBigWig file
        bw = pyBigWig.open(bigwig_file, "w")
        # Set the chromosome lengths in the bigWig file
        bw.addHeader([("chr1", 248956422),
                      ("chr2", 242193529),
                      ("chr3", 198295559),
                      ("chr4", 190214555),
                      ("chr5", 181538259),
                      ("chr6", 170805979),
                      ("chr7", 159345973),
                      ("chr8", 145138636),
                      ("chr9", 138394717),
                      ("chr10", 133797422),
                      ("chr11", 135086622),
                      ("chr12", 133275309),
                      ("chr13", 114364328),
                      ("chr14", 107043718),
                      ("chr15", 101991189),
                      ("chr16", 90338345),
                      ("chr17", 83257441),
                      ("chr18", 80373285),
                      ("chr19", 58617616),
                      ("chr20", 64444167),
                      ("chr21", 46709983),
                      ("chr22", 50818468),
                      ("chrX", 156040895)]
                     )
        # skip header
        for line_number, line in enumerate(updated_lines, 1):

            bin_index, chr_index, adjbin_mb = line.strip().split(",")
            bin_index = int(bin_index)
            # print()
            # print(f"{[chr_index]}, {[start]}, ends={[end]}, values={[adjbin_mb]}")
            adjbin_mb = float(adjbin_mb)

            # Calculate start and end positions based on bin index and bin size
            # 0-9999,10000-19999
            start = bin_index * bin_size
            end = start + bin_size - 1
            # print(f"{[chr_index]}, {[start]}, ends={[end]}, values={[adjbin_mb]}")
            bw.addEntries([chr_index], [start], ends=[end], values=[adjbin_mb])
        # Close the bigWig file
        bw.close()

    # Main configurations
    # Mapping of index to extensions
    extension_mapping = {
        9: "sigma_bin_mb_totalSD_log2",
        10: "sigma_bin_mb_totalSD_sm_log2",
        11: "sigma_bin_mb_totalSD_tm_log2",
        13: "adjbin_mb",
        14: "adjbin_SD",
        6: "adjust",
        15: "bin",
        12: "adjbin",
        8: "sigma_bin_totalSD",
        4: "sigma_bin_totalSD_sm",
        5: "sigma_bin_totalSD_tm",
        7: "sigma_bin_mb_totalSD",
        2: "sigma_bin_mb_totalSD_sm",
        3: "sigma_bin_mb_totalSD_tm"
    }
    # Generate new filenames
    bigwig_filenames = []
    # Remove the '.csv' extension from the base filename
    base_file_name = csv_file.rsplit('.', 1)[0]

    for index, extension in extension_mapping.items():
        bw_filename = f"{base_file_name}_{extension}.bw"
        bigwig_filenames.append(bw_filename)

    # Corresponding chosen values
    chosen_values = [9, 10, 11, 13, 14, 6, 15, 12, 8, 4, 5, 7, 2, 3]

    # Pair the filenames with the chosen values
    configs = zip(bigwig_filenames, chosen_values)

    # Loop over configurations and generate BigWig files
    for bigwig_file, choosen_value in configs:
        generate_bigwig_file(bigwig_file, choosen_value,
                             csv_file, chrom_mapping, bin_size)

elif args.scripts == 'peak_annotation':

    # Read HU samples from CSV files

    def read_peak_samples_csv(file_path, col_index, skip_first=False):
        headers = ["chr" + str(i) for i in range(1, 23)] + ["chrX"]
        try:
            # Initialize an empty list to store all rows
            all_rows = []

            with open(file_path, 'r') as file:
                reader = csv.reader(file)
                if skip_first:
                    next(reader)  # Skip the first row
                for row in reader:
                    selected_values = [float(row[col_index-1 + (i * 15)]) if row[col_index-1 + (
                        i * 15)] not in ('', 'NA', 'N/A') else 0.0 for i in range(23)]
                    all_rows.append(selected_values)

            # Convert the list of lists into a DataFrame with the desired headers
            result_df = pd.DataFrame(all_rows, columns=headers)

            return result_df
        except FileNotFoundError:
            v_print(args.verbose, 1, f"File {file_path} not found.")
            return None

    def describe_all(df):
        """Describe statistics for all numeric columns in the DataFrame.

        Parameters:
            df (DataFrame): Data to analyze.

        Returns:
            dict: A dictionary containing statistics for all numeric columns.
        """
        summary_dict = {}

        for column_name in df.select_dtypes(include=[np.number]).columns:
            data = df[column_name]

            minimum = np.min(data)
            q25 = np.percentile(data, 25)
            mean = np.mean(data)
            median = np.median(data)
            q75 = np.percentile(data, 75)
            maximum = np.max(data)

            summary_dict[column_name] = {
                "min": minimum,
                "25%": q25,
                "mean": mean,
                "median": median,
                "75%": q75,
                "max": maximum
            }

        summary_all = {
            "min": min([v['min'] for v in summary_dict.values()]),
            "25%": np.percentile([v['25%'] for v in summary_dict.values()], 25),
            "mean": np.mean([v['mean'] for v in summary_dict.values()]),
            "median": np.median([v['median'] for v in summary_dict.values()]),
            "75%": np.percentile([v['75%'] for v in summary_dict.values()], 75),
            "max": max([v['max'] for v in summary_dict.values()]),
        }

        return summary_all

    # The prominence threshold is a measure of how much a peak stands out from its surroundings.
    # A higher prominence threshold will result in fewer, more significant peaks, while a
    # lower threshold will detect more peaks, including smaller ones.
    def find_peaks_in_dataframe(df, prominence_threshold):
        # Initialize a list to store the peaks for all chromosomes
        peaks_list = []
        # Loop through each column in the DataFrame
        for col in df.columns:
            data = df[col].values

            # Find peaks
            peaks_indices = find_peaks(
                data, prominence=prominence_threshold)[0]
            peaks_values = [data[j] for j in peaks_indices]

            # Create a list of data for this chromosome's peaks
            temp_data = list(
                zip([col] * len(peaks_indices), peaks_indices, peaks_values))

            # Extend the master list with this chromosome's peaks data
            peaks_list.extend(temp_data)

            # Convert the list of peaks data to a DataFrame
        peaks_df = pd.DataFrame(peaks_list, columns=[
                                'Chromosome', 'Peak_Position', 'Peak_Value'])

        return peaks_df

    # read in the peak sample with EDU + Treatment
    # here we read in the adjbin values
    # this decision is based on the description in
    # Nature: "intragenic origins due to short G1 phases underlie oncogene-induced DNA replication stress"
    EdUseq_HU_4h_OE = read_peak_samples_csv(
        args.csv_file, 14, skip_first=False)
    # read the reference sample which is EDU without treatment
    headers = ["chr" + str(i) for i in range(1, 23)] + ["chrX"]
    # on the NE sample  we read in the normal BIN values
    EdUseq_14h_NE = read_peak_samples_csv(args.ref_file, 14, skip_first=False)
    # Identify peaks
    peaks_HU_4h_OE_bin = find_peaks_in_dataframe(
        EdUseq_HU_4h_OE, args.prominence_threshold)

    # Detecting peaks in the reference sample using the same prominence threshold
    peaks_14h_NE_bin = find_peaks_in_dataframe(
        EdUseq_14h_NE, args.prominence_threshold)

    v_print(args.verbose, 1,
            f"Total number of entries Edu + Treatment: {peaks_HU_4h_OE_bin.shape[0]}")
    v_print(args.verbose, 1,
            f"Total number of entries Edu - Treatment: { peaks_14h_NE_bin.shape[0]}")

    # The resulting dictionary for the 4h OE (Over Expression | (Oi) oncogene inducing) experiment,
    # named Peak dictionary for 4h OE, describes the peaks identified in the
    # sequencing data for chromosome 1 ("chr1"). Each peak is represented by a tuple containing two elements:

    # The position of the peak along the chromosome: This is essentially an index or coordinate along
    # the chromosome where a peak in the data is observed.
    # The sigma value of the peak: This is a measure of the height or intensity
    # of the peak, which usually represents some biologically meaningful property like the
    # frequency or concentration of a particular molecule, such as an RNA or a protein, at that location.

    # So, for example, the first entry (136, 1080.0) for "chr1" means that there is a peak at position
    # 136 along chromosome 1, and the height or intensity of this peak, represented by the sigma value, is 1080.0.
    # Initialize variables to store the sum of shared peaks sigma
    total_shared_peaks_sigma_4h_OE = 0
    total_shared_peaks_sigma_14h_NE = 0
    # Initialize variables to store the sum of shared peaks sigma
    total_shared_peaks_sigma_4h_OE = 0
    total_shared_peaks_sigma_14h_NE = 0

    # Loop through each chromosome in 4h OE dictionary
    for chrom in peaks_HU_4h_OE_bin['Chromosome'].unique():
        peak_df_4h_OE = peaks_HU_4h_OE_bin[peaks_HU_4h_OE_bin['Chromosome'] == chrom]
        peak_df_14h_NE = peaks_14h_NE_bin[peaks_14h_NE_bin['Chromosome'] == chrom]

        # Find shared peak positions
        shared_peaks_positions = set(peak_df_4h_OE['Peak_Position']).intersection(
            set(peak_df_14h_NE['Peak_Position']))

        # Calculate sum for AdjFactor based on shared peaks
        total_shared_peaks_sigma_4h_OE += peak_df_4h_OE[peak_df_4h_OE['Peak_Position'].isin(
            shared_peaks_positions)]['Peak_Value'].sum()
        total_shared_peaks_sigma_14h_NE += peak_df_14h_NE[peak_df_14h_NE['Peak_Position'].isin(
            shared_peaks_positions)]['Peak_Value'].sum()

    # Calculate constant AdjFactor based on the subset of selected peaks
    v_print(args.verbose, 1,
            f"total_shared_peaks of EdU + Treatment: { total_shared_peaks_sigma_4h_OE}")
    v_print(args.verbose, 1,
            f"total_shared_peaks of EdU - Treatment: {total_shared_peaks_sigma_14h_NE}")

    AdjFactor = total_shared_peaks_sigma_14h_NE / \
        total_shared_peaks_sigma_4h_OE if total_shared_peaks_sigma_4h_OE != 0 else 0
    v_print(args.verbose, 1,
            f"Only peak based adjustmentfactor: {AdjFactor}")

    # If two peaks from different dataframes are adjacent to each other,
    # we'll keep the position of the one with the higher sigma value.
    # For this chosen position, we will assign both sigma values (from 4h OE and 14h NE).
    #
    # If two peaks from different dataframes have the exact same position,
    # we'll keep the position and assign both sigma values to it.

    # For peaks present in only one sample, we'll keep the position and assign its
    # sigma value. For the other sample, where the peak is not detected,
    # we'll fetch the value from the original data for that position

    def assign_peaks(merged_df):

        # Iterate over the combined dataframe to merge adjacent peaks and assign values
        merged_peaks = []
        prev_row = None
        for idx, row in merged_df.iterrows():
            if prev_row is None:
                prev_row = row
                continue

            # If the current peak and previous peak are from different dataframes and on the same chromosome
            if (row['Chromosome'] == prev_row['Chromosome'] and
                    row['source'] != prev_row['source']):

                # If the peaks are adjacent or at the same position take the higher normalized peak
                if abs(row['Peak_Position'] - prev_row['Peak_Position']) <= 1:
                    # Determine which peak to retain based on sigma value
                    if row['Edu_norm'] > prev_row['Edu_norm']:
                        # Assign values from both dataframes
                        merged_peak = {
                            'Chromosome': row['Chromosome'],
                            'Peak_Position': row['Peak_Position'],
                            'EP_peak_values': row['Edu_peak_value'],
                            'EP_peak_norm_values': row['Edu_norm'],
                            'WT_peak_values': row['WT1_peak_value']
                        }
                    else:
                        # Assign values from both dataframes
                        merged_peak = {
                            'Chromosome': prev_row['Chromosome'],
                            'Peak_Position': prev_row['Peak_Position'],
                            'EP_peak_values': prev_row['Edu_peak_value'],
                            'EP_peak_norm_values': prev_row['Edu_norm'],
                            'WT_peak_values': prev_row['WT1_peak_value']
                        }
                    merged_peaks.append(merged_peak)
                    prev_row = None  # Reset prev_row so that it doesn't get added again in the next iteration
                    continue
            merged_peak = {
                'Chromosome': row['Chromosome'],
                'Peak_Position': row['Peak_Position'],
                'EP_peak_values': row['Edu_peak_value'],
                'EP_peak_norm_values': row['Edu_norm'],
                'WT_peak_values': row['WT1_peak_value']
            }
            merged_peaks.append(merged_peak)
            prev_row = row

        # Convert the merged peaks list to a DataFrame
        merged_df = pd.DataFrame(merged_peaks)
        # Calculate the ratio, forgo that fraction /0 can happen, need to take care of that case
        # this was not the case when we used the bandaid binning csv
        # we add an offset of 0.1 for now
        offset_ratio = 1
        merged_df['Ratio'] = merged_df['EP_peak_norm_values'] / \
            (merged_df['WT_peak_values'] + offset_ratio)
        pprint.pprint(merged_df)
        merged_df.to_csv("test.csv")
        # Categorize based on the ratio
        merged_df['Category'] = np.where(merged_df['Ratio'] < 2, 'NE', 'EP')

        # In case the last row hasn't been added
        if prev_row is not None:
            new = pd.DataFrame([{
                'Chromosome': prev_row['Chromosome'],
                'Peak_Position': prev_row['Peak_Position'],
                'EP_peak_values': prev_row['Edu_peak_value'],
                'EP_peak_norm_values': prev_row['Edu_norm'],
                'WT_peak_values': prev_row['WT1_peak_value'],
                'Ratio': prev_row['Edu_norm']/prev_row['WT1_peak_value'],
                'Category': np.where(prev_row['Edu_norm']/prev_row['WT1_peak_value'] < 2, 'NE', 'EP')
            }])
            merged_df = pd.concat([merged_df, new], ignore_index=True)
        sorted_df = merged_df.sort_values(by='Ratio', ascending=False)
        return sorted_df

    # Using the function
    Adjf_peaks_Edu1 = peaks_HU_4h_OE_bin.copy()
    Adf_peaks_WT1 = peaks_14h_NE_bin.copy()
    # Iterate through the rows of Adjf_peaks_Edu1 and get the corresponding values from EdUseq_14h_NE
    # get both peak values before merging the peaks

    Adjf_peaks_Edu1['WT1_peak_value'] = Adjf_peaks_Edu1.apply(
        lambda row: EdUseq_14h_NE.at[row['Peak_Position'], row['Chromosome']], axis=1)
    Adjf_peaks_Edu1.rename(
        columns={'Peak_Value': 'Edu_peak_value'}, inplace=True)
    Adjf_peaks_Edu1['source'] = "Edu"

    Adf_peaks_WT1['Edu_peak_value'] = Adf_peaks_WT1.apply(
        lambda row: EdUseq_HU_4h_OE.at[row['Peak_Position'], row['Chromosome']], axis=1)
    Adf_peaks_WT1.rename(
        columns={'Peak_Value': 'WT1_peak_value'}, inplace=True)
    Adf_peaks_WT1['source'] = "WT1"

    merged_df = pd.concat(
        [Adjf_peaks_Edu1, Adf_peaks_WT1], ignore_index=True)
    merged_df = merged_df.sort_values(
        by=['Chromosome', 'Peak_Position'])

    merged_df.to_csv("Merged_peakset_preduplicatedrop.csv", index=False)
    merged_df = merged_df.drop_duplicates(
        subset=['Chromosome', 'Peak_Position'], keep='first')

    # Normalize and round
    # here we dont have the reference norm bins anymore but actually normalized bins allready
    # just add a small offset and thats it
    # merged_df['Edu_norm'] = merged_df['Edu_peak_value'].div(
    #    merged_df["WT1_peak_value"]) * AdjFactor * 1000
    # therefore edu norm is just the normal adjust peakvalue at the 5th column we read in
    merged_df['Edu_norm'] = merged_df['Edu_peak_value']*AdjFactor
    # merged_df.to_csv("Merged_peakset.csv", index=False)

    # Rounding function
    # This method uses multiplication and division to shift the decimal point,
    # enabling custom rounding to the desired number of decimal places.

    def custom_round(value, decimal_places=0):
        """
        Custom rounding function.
        Rounds up for values >= .5 and rounds down for values < .5.
        """
        if pd.notna(value):  # Check if the value is not NaN
            multiplier = 10 ** decimal_places
            value *= multiplier
            base_value = int(value)
            decimal_part = value - base_value

            if decimal_part >= 0.5:
                rounded_value = base_value + 1
            else:
                rounded_value = base_value

            return rounded_value / multiplier
        else:
            return value  # Return NaN as is

    # Adjf_peaks_Edu1['Peak_Value'] = Adjf_peaks_Edu1['Peak_Value'].apply(
     #   custom_round)

    # peak_df_1, peak_df_2, original_df_1, original_df_2
    merged_peaks_df = assign_peaks(merged_df)
    merged_peaks_df['Ratio'] = merged_peaks_df['Ratio'].apply(
        custom_round, decimal_places=2)
    merged_peaks_df['EP_peak_norm_values'] = merged_peaks_df['EP_peak_norm_values'].apply(
        custom_round, decimal_places=0)

    v_print(args.verbose, 1,
            f"Total number of entries Edu + Treatment: {peaks_HU_4h_OE_bin.shape[0]}")
    v_print(args.verbose, 1,
            f"Total number of entries Edu - Treatment: { peaks_14h_NE_bin.shape[0]}")
    v_print(args.verbose, 1,
            f"Total number of peaks after sucessfull merging: { merged_peaks_df.shape[0]}")

    # Annotate peaks with nearest genes

    # Initialize pyensembl.EnsemblRelease() to access the gene annotations.
    #
    # For each row in the dataframe, calculate the start and end positions based on
    # the Peak_Position column and the bin size.
    #
    # Fetch genes for each Peak_Position by considering a window of 10,000 base pairs on both sides (left and right).
    # Also add the genes at the 10k bin peakposition to have both
    #
    # if no gene names are available add the gene_ids
    # Extract the gene names and add them to the dataframe as a new column.
    # Initialize EnsemblRelease
    ensembl = EnsemblRelease()

    def get_gene_representation(gene):
        """Returns the gene name if available; otherwise, returns the gene ID."""
        return gene.gene_name if gene.gene_name else gene.gene_id

    def annotate_genes(row, left_extension=10000, right_extension=10000):
        # Calculate start and end positions based on Peak_Position and bin size

        start = ((row['Peak_Position']) * 10000) - left_extension
        end = (row['Peak_Position'] * 10000) + 9999 + right_extension

        # Fetch genes across the entire range
        genes_in_range = ensembl.genes_at_locus(
            contig=row['Chromosome'].replace('chr', ''), position=start, end=end)

        # Extract gene names or IDs
        gene_names_or_ids = [get_gene_representation(
            gene) for gene in genes_in_range]

        # Create a semi-colon separated string of unique gene names or IDs
        unique_gene_names_or_ids = list(set(gene_names_or_ids))

        return ';'.join(unique_gene_names_or_ids)

        # Create a semi-colon separated string
        return ';'.join(unique_gene_names_or_ids)

    def fetch_peak_genes(row):
        # Calculate start and end positions based on Peak_Position and bin size
        start = row['Peak_Position'] * 10000
        end = start + 9999

        # Fetch genes overlapping the peak bin
        peak_genes = ensembl.genes_at_locus(
            contig=row['Chromosome'].replace('chr', ''), position=start, end=end)

        # Extract gene names or IDs
        peak_gene_names_or_ids = [
            get_gene_representation(gene) for gene in peak_genes]

        # Create a semi-colon separated string
        return ';'.join(peak_gene_names_or_ids)

    # Create a partial function with desired extension values
    annotate_with_extensions = partial(
        annotate_genes, left_extension=10000, right_extension=10000)

    # Annotate the dataframe with  progress bar
    tqdm.pandas(desc="Annotating Nearest Genes")
    merged_peaks_df['Nearest_Genes'] = merged_peaks_df.progress_apply(
        annotate_with_extensions, axis=1)
    tqdm.pandas(desc="Fetching Peak Genes")
    merged_peaks_df['Peak_Genes'] = merged_peaks_df.progress_apply(
        fetch_peak_genes, axis=1)

    # shortcut, reference copy
    edu_df = merged_peaks_df

    # Add 'chr' prefix to fragile sites DataFrame
    fragile_sites_df = pd.read_excel(args.fragile_sites)
    fragile_sites_df['Chr'] = 'chr' + fragile_sites_df['Chr'].astype(str)

    edu_df['Start'] = (edu_df['Peak_Position']) * 10000
    edu_df['End'] = edu_df['Peak_Position'] * 10000 + 9999

    def annotate_edu_with_cfs(edu_df, fragile_df):
        # Initialize new columns in edu_df
        edu_df['Fragile_overlap'] = None
        edu_df['Fragile_Chr'] = None
        edu_df['Fragile_Start'] = None
        edu_df['Fragile_End'] = None
        edu_df['Closest_Gene_CFS'] = None

        edu_grouped = edu_df.groupby('Chromosome')
        fragile_grouped = fragile_df.groupby('Chr')

        # Loop through each chromosome group in edu_df
        for chr_name, edu_group in tqdm(edu_grouped, desc="Processing Chromosomes"):
            if chr_name not in fragile_grouped.groups:
                continue
            fragile_group = fragile_grouped.get_group(chr_name)

            for edu_index, edu_row in edu_group.iterrows():
                edu_start = edu_row['Start']
                edu_end = edu_row['End']

                # Check overlap with each CFS site in the fragile_group
                for _, fragile_row in fragile_group.iterrows():
                    fragile_start = fragile_row['MiDASstart']*10000
                    fragile_end = fragile_row['MiDASend']*10000

                    if edu_end >= fragile_start and edu_start <= fragile_end:
                        # Update edu_df with CFS details
                        edu_df.at[edu_index,
                                  'Fragile_Site'] = fragile_row["CFS/IntergenicAnnotation"]
                        edu_df.at[edu_index,
                                  'Fragile_Chr'] = fragile_row['Chr']
                        edu_df.at[edu_index, 'Fragile_Start'] = fragile_start
                        edu_df.at[edu_index, 'Fragile_End'] = fragile_end
                        edu_df.at[edu_index,
                                  'Closest_Gene_CFS'] = fragile_row['Closestgene']
                        edu_df.at[edu_index, 'Fragile_overlap'] = "yes"
                        break

        return edu_df

    def midas_start_summary(df):
        column_data = df['MiDASstart']
        summary = {
            'Min': column_data.min(),
            'Max': column_data.max(),
            'Mean': column_data.mean(),
            'Median': column_data.median(),
            'Standard Deviation': column_data.std()
        }
        return summary

    # Using the function
    summary = midas_start_summary(fragile_sites_df)
    # print(summary)
    # Annotate edu_df with overlapping CFS details
    edu_df = annotate_edu_with_cfs(edu_df, fragile_sites_df)

    def format_peak_position(row):
        start = row['Peak_Position'] * 10000
        end = start + 9999
        return f"{row['Chromosome']}:{start}-{end}"

    def format_extended_range(row):
        start = (row['Peak_Position'] * 10000) - 2500000
        end = (row['Peak_Position'] * 10000) + 9999 + 2500000
        return f"{row['Chromosome']}:{start}-{end}"

    edu_df['Exact_Peak_Position'] = edu_df.apply(
        format_peak_position, axis=1)
    edu_df['Extended_Range'] = edu_df.apply(format_extended_range, axis=1)
    base_file_name = args.csv_file.rsplit('.', 1)[0]
    edu_df.sort_values(by='Ratio', ascending=False, inplace=True)
    edu_df.to_csv(
        f"{base_file_name}_prom{args.prominence_threshold}_annotated_cfs_peaks.csv", index=False)
    v_print(args.verbose, 1,
            f"{base_file_name}_prom{args.prominence_threshold}_annotated_cfs_peaks.csv written...")

    # also create a bed file so we can use homers motif search

    # Select and reorder columns for the BED file
    filtered_df = edu_df[edu_df['Category'] == 'EP']
    filtered_df['Peak_Position'] = filtered_df['Chromosome'] + \
        '_' + filtered_df['Peak_Position'].astype(str)
    bed_df = filtered_df[['Chromosome', 'Start', 'End', 'Peak_Position']]

    def create_bed_dataframe_by_ratio(csv_df, ratio):
        # Create new columns initialized with NaNs
        csv_df['Start'] = pd.NA
        csv_df['End'] = pd.NA

        # Apply the calculation only to rows where 'Category' is 'EP'
        condition = csv_df["Ratio"] >= ratio
        csv_df.loc[condition, 'Start'] = (
            csv_df.loc[condition, 'Peak_Position'] * 10000)
        csv_df.loc[condition, 'End'] = (
            csv_df.loc[condition, 'Peak_Position'] * 10000 + 9999)
        csv_df.loc[condition, 'Ratio'] = (csv_df.loc[condition, 'Ratio'])

        # Select only relevant columns for BED format and drop rows without Start and End
        bed_df = csv_df[['Chromosome', 'Start', 'End', 'Ratio']].dropna()

        return bed_df
    bed_df = create_bed_dataframe_by_ratio(edu_df, args.ratio)
    bed_df['Unused'] = '.'  # Column 5 that's not used
    bed_df['Strand'] = '+'  # Column 6, strand set to '+'

    # Save the BED DataFrame to a .bed file
    bed_df.to_csv(
        f"{base_file_name}_prom{args.prominence_threshold}_annotated_cfs_peaks.bed", sep="\t", index=False)
    v_print(args.verbose, 1,
            f"{base_file_name}_prom{args.prominence_threshold}_annotated_cfs_peaks.bed written...")

else:
    v_print(args.verbose, 1, "No subscript is running...")
    parser.print_help()
