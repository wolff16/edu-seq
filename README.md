# EdU-Seq Pipeline

![Flowchart of the analysis of Edu-Seq samples](data/eduseq_workflow.jpeg "Overview of the analysis of Edu-seq samples")
## Introduction

*EdU-Seq* is part of a bioinformatical pipeline for the analysis and visualisation of edu-labeled nascent DNA to detect origin fireing, called after this method EdU-Seq. 
It is based on the [nature-protocol](https://www.nature.com/articles/s41596-018-0081-y) and [publication with perl-scripts](https://www.nature.com/articles/nature25507) of Morgane Macharet & Thanos D. Halazonetis, but reimplemented in Python. The idea of identification and classification of origin firing positions was taken from [Macheret M, Halazonetis TD](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5837010/). Intragenic origins due to short G1 phases underlie oncogene-induced DNA replication stress. Nature. 2018 Mar 1;555(7694):112-116. doi: 10.1038/nature25507. For visualization the [JBrowse](https://jbrowse.org/jb2/) can be used.

Prerequisites are indexed bam files from the background reference of the used cell line, as well as your edu labeled bam files for which you want to detect the origins. In the following paragraph I present some recommendations for creating such a bam file.

Usually following steps are done:

* Quality Control your fastq file with ```fastqc``` and ```fastq-screen``` and do a multiqc-report file
* reference indexing and alignment of the quality controled fastq-files via ```bwa-mem```
* index .bam file with ```samtools index```

## Quick Guide

Here are some handy bash scripts I used for the creation of Edu-seq samples:

```1_fastqc.sh```
```bash
#!/bin/bash

# Specify the input folder containing the fastq.gz files
input_folder="/media/awolff/DATA1/Projects/Eduseq_invest/fastq"

# Specify the output folder to store the FastQC results
output_folder="/media/awolff/DATA1/Projects/Eduseq_invest/fastqc"

# Run FastQC for each fastq.gz file in parallel
ls "$input_folder"/*.fastq.gz | parallel -j 1 '/opt/fastqc/fastqc_v0.11.9/FastQC/fastqc --threads 2 {} --outdir="$output_folder"'
```
```2_bwa_index```
```bash
# index bwa mem
/opt/bwa/bwa index /media/hdd2/PacBio/Genome/bwa_mem2/GRCh38_with_chr_alignment.fa
[bwa_index] Pack FASTA... 
...
```

```2_bwa_mapping.sh```
```bash
#!/usr/bin/env bash

# EdU-Seq Pipeline

fastq_dir="/path/to/fastq_dir"
log_dir="/path/to/log_dir"

# Create log directory if it doesn't exist
mkdir -p "$log_dir"

# Find all single-end fastq files in the directory
fastq_files=$(find "$fastq_dir" -name '*.fastq.gz')

# Reference genome
reference="/path/to/reference.fa"

# Loop over each fastq file
for fastq in $fastq_files; do
    
    # Construct output filenames
    sorted_bam=${fastq%.fastq.gz}_sorted.bam
    log_file=${log_dir}/$(basename "${fastq%.fastq.gz}")_log.txt

    echo "Aligning $fastq"
    
    # 1) Align reads using bwa mem, capturing stderr in a log file
    # 2) Pipe directly to samtools for sorting
    /opt/bwa/bwa mem -t 2 "$reference" "$fastq" 2> "$log_file" \
    | samtools sort -o "$sorted_bam"

    # Index the sorted BAM file
    samtools index "$sorted_bam"

done
```

Some metrics of the aligned bam files, add your tools and scripts here as well:

```3_mapping_stats.sh```
```bash
#!/bin/bash

# Set the directory containing fastq files
fastq_dir="/media/awolff/DATA1/Projects/Eduseq_invest/fastq"

# Create the "bam" directory
bam_dir=$(dirname "$fastq_dir")/bam
mkdir -p "$bam_dir"

# Create a directory for log files
log_dir=$(dirname "$fastq_dir")/logs
mkdir -p "$log_dir"

#!/bin/bash
# Set the directory containing fastq files
fastq_dir="/media/awolff/DATA1/Projects/Eduseq_invest/fastq"
# Set the directory containing BAM files
bam_dir=$(dirname "$fastq_dir")/bam

# Set the output directory for metrics files
metrics_dir=$(dirname "$fastq_dir")/metrics

# Path to Picard Tools
picard_jar="/opt/Picard_2_25_2/picard.jar"

# Path to reference genome FASTA file
reference="/media/awolff/Archieve/Projects/Annotation/hg38/hg38_soe_ucsc_chr_names.fa"

# Create the output directory if it doesn't exist
mkdir -p "$metrics_dir"

# Loop over each BAM file in the directory
for bam_file in "$bam_dir"/*.bam; do
    # Generate the output metrics file name
    metrics_file="$metrics_dir/$(basename "${bam_file%.*}").metrics"
    
    # Run CollectAlignmentSummaryMetrics
    java -jar "$picard_jar" CollectAlignmentSummaryMetrics \
         R="$reference" \
         I="$bam_file" \
         O="$metrics_file"
done
``` 


Feel free to sum all log files into one big multiqc report via ```multiqc .```

Now you can use the provided Python-script for reference preparation, binning and peak-calling.


Here are some examples calls:

Prepare the Reference file for the specific cellline u are using, therefore
the inout file needs ti be an alignment WGS sample:
* ```python3.12 detect_EdU_peaks.py --verbose 1 prepare_reference -b ../Alignment/HCT116-WT-WGS-rep1.sorted.bam -t 12 --read_length 150 --read_length_offset 50```

Next the binning of the EDU WT and EDU Treatment sample takes place:
* ``` python3.12 detect_EdU_peaks.py --verbose 1 binning -b ../Alignment/HCT116-WT-EDU-rep1.sorted.bam -a HCT116-WT-WGS-rep1-Reference_adjust_bin-size_10000_chr1-X_adjbin_0b.csv -t 12 --read_length 150 --read_length_offset 50 -d GRCh38```
* ``` python3.12 detect_EdU_peaks.py --verbose 1 binning -b ../Alignment/HCT116-SMAD4_EDU-rep1.sorted.bam -a HCT116-WT-WGS-rep1-Reference_adjust_bin-size_10000_chr1-X_adjbin_0b.csv -t 12 --read_length 150 --read_length_offset 50 -d GRCh38```

Aswell, as the peakcalling of the corresponding bin files
* ```python3 detect_EdU_peaks.py -v 1 peak_calling -q bam/HCT116-WT-EDU-rep1_sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_qual_counts_0b.txt -f bam/HCT116-WT-EDU-rep1.sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_0b.csv -b bam/HCT116-WT-EDU-rep1.sorted.bam```
* ```python3 detect_EdU_peaks.py -v 1 peak_calling -q bam/HCT116-SMAD4_EDU-rep1.sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_qual_counts_0b.txt -f bam/HCT116-SMAD4_EDU-rep1.sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_0b.csv -b bam/HCT116-SMAD4_EDU-rep1.sorted.bam```

* ```./detect_EdU_peaks.py  2bw -f bam/HCT116-WT-EDU-rep1_sorted.bam_sigma_all_0b_long_sorted.csv```
* ```./detect_EdU_peaks.py  2bw -f bam/HCT116-SMAD4_EDU-rep1.sorted.bam_sigma_all_0b_long_sorted.csv```

Here please use your reference normalized file like WT1-EDU as the -r parameter and use your treatment file like SMAD4-EDU as .f file:
* ```python3 detect_EdU_peaks.py peak_annotation -f bam/HCT116-SMAD4_EDU-rep1.sorted.bam_sigma_all_0b.csv -c data/Midas-seq-CFS-united20230710.xlsx -r bam/HCT116-WT-EDU1-rep1.sorted.bam_sigma_all_0b.csv -p 100```

After peak annotation there will be a ..._sigma_all_0b_promX_annotated_cfs_peaks.csv in the same folder as the .bam file with gene and common fragile sites overlap:
The corresponding .bed file can be loaded as a track. It has all the peaks marked with ratios higher 4 (default).
| Chromosome | Peak_Position | EP_peak_values | WT_peak_values | Ratio | Category | Nearest_Genes                                                                | Peak_Genes                        | Start    | End      | Fragile_overlap | Fragile_Chr | Fragile_Start | Fragile_End | Closest_Gene_CFS | Fragile_Site | Exact_Peak_Position     | Extended_Range          |
|------------|---------------|----------------|----------------|-------|----------|------------------------------------------------------------------------------|-----------------------------------|----------|----------|-----------------|-------------|---------------|-------------|------------------|--------------|-------------------------|-------------------------|
| chr11      | 1050          | 22353          | 2343           | 9.54  | EP       | RNF141;MIR4485;AMPD3                                                         | AMPD3;MIR4485                     | 10500000 | 10509999 |                 |             |               |             |                  |              | chr11:10500000-10509999 | chr11:8000000-13009999  |
| chr16      | 299           | 7743           | 913            | 8.47  | EP       | ENSG00000272079;GREP1;ENSG00000274367;ENSG00000289281;PKMYT1;ENSG00000262362 | GREP1                             | 2990000  | 2999999  |                 |             |               |             |                  |              | chr16:2990000-2999999   | chr16:490000-5499999    |
| chr16      | 56            | 1808           | 245            | 7.35  | EP       | PRR35;CAPN15;PIGQ;ENSG00000282907;NHLRC4;ENSG00000261691                     | PIGQ;PRR35;NHLRC4;ENSG00000282907 | 560000   | 569999   |                 |             |               |             |                  |              | chr16:560000-569999     | chr16:-1940000-3069999  |
| chr17      | 8136          | 8849           | 1260           | 7.02  | EP       | LINC03048;ENSG00000263154                                                    | ENSG00000263154                   | 81360000 | 81369999 |                 |             |               |             |                  |              | chr17:81360000-81369999 | chr17:78860000-83869999 |
| chr5 | 10005 | 47173 | 2236 | 21.09 | EP | MTCO3P22;MTATP6P2;MTND4LP5;MTCYBP22;MTND4P35;MTND5P10;MTCO2P22;MTND6P22;GUSBP7;MTCO1P22;MTND3P19 | MTCO2P22;MTATP6P2;MTND4LP5;MTCO3P22;MTND4P35;MTCO1P22;MTND3P19 | 100050000 | 100059999 |     |      |          |          |          |                 | chr5:100050000-100059999 | chr5:97550000-102559999 |
| chr5 | 9457  | 21695 | 1154 | 18.78 | EP | KIAA0825;MTCYBP35;MTND6P3;MTND5P12                                                               | KIAA0825;MTCYBP35                                              | 94570000  | 94579999  | yes | chr5 | 93700000 | 94690000 | KIAA0825 | FRA5B and FRA5D | chr5:94570000-94579999   | chr5:92070000-97079999  |



Meaning of columns
* Chromosome: chromosome name of the origin site location
* Peak_Position: the actual peak position based on Macharet et al, Nature 2018 nomenclature, here 0 would be a binsize of 0-9999 bases
* EP_peak_values: normalized eduseq peak (EP) values (raw_edu_bin_counts*adjustmentfactor)
* WT_peak_values: raw reference bin counts 
* Ratio: EP_peak_values/WT_peak_values
* Category: EP (Eduseq peaks, if ratio>=4) 
* Nearest_Genes: if a gene annotation from ensembl overlaps +-1 bins of the peak position                                 
* Peak_Genes: annotated genes overlapping at the exact peak position
* Start: start position of the Peak_position
* End: end position of the Peak_position
* Fragile_overlap: logical "yes" for easier subseting in excel
* Fragile_Chr: the original information from the CFS annotation
* Fragile_Start: the original information from the CFS annotation, used defining the overlap with the Peak_position
* Fragile_End: the original information from the CFS annotation, used defining the overlap with the Peak_position
* Closest_Gene_CFS: the original information from the CFS annotation
* Fragile_Site: description of the Fragile site
* Exact_Peak_Position: the exact peak position in chromosome coordinates, easy to copy and paste for jbrowse
* Extended_Range: the exact centered peak position in chromosome coordinates with a larger surrounding landscape, easy to copy and paste for jbrowse

 Conversion of peak calling results (.wig) into bigwig format 
* ```./detect_EdU_peaks.py  2bw -f bam/Edu1_S2_R1_001_sorted_subsampled_0.001.bam_sigma_all_0b_long_sorted.csv```
* visualisation via [JBrowse](https://jbrowse.org/jb2/download/) by using the resulting .bw files.


The processing of the data needs to be done under Linux. But for the visualization and interactive exploration of peaks you can use MacOS, Windows or Linux. All you need is your bigwig files and the installed JBrowse-browser.


# Installation

## Install required software

**python packages**

* numpy
* pandas
* pyBigWig
* pysam
* pyensembl

You can do a user-specific install using the included requirements.txt file in the pipeline directory:

```pip install --user --upgrade -r requirements.txt```

**Required executables**. you will need some common bioinformatics tools installed. The following tools are a recomended prerequisite for creating the alignment file (.bam) as an input for the python-script:

  * [FastQC 0.12.0](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
  * [fastq-screen](https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/)
  * [multiqc](https://multiqc.info/)
  * [bbmap (v2.0+)](https://jgi.doe.gov/data-and-tools/software-tools/bbtools/bb-tools-user-guide/bbmap-guide/)
  * [bwa (0.7.17+)](https://bio-bwa.sourceforge.net/)
  * [bedtools (v2.25.0+)](https://bedtools.readthedocs.io/en/latest/content/installation.html)
  * [samtools (v1.7)](https://github.com/samtools/samtools/releases/download/1.17/samtools-1.17.tar.bz2)


### **FastQC**

```
   wget https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.12.1.zip
   sudo unzip fastqc_v0.12.1.zip
   chmod 755 FastQC/fastqc
   sudo ln -s /opt/FastQC/fastqc /usr/local/bin/fastqc
```
### **Fastq-Screen**

```
   wget https://github.com/StevenWingett/FastQ-Screen/archive/refs/tags/v0.15.3.zip
   sudo unzip v0.15.3.zip
   sudo mv /home/awolff/Documents/v0.15.3/FastQ-Screen-0.15.3 /opt/FastQ-Screen/
   chmod 755 /opt/FastQ-Screen/fastq_screen
   sudo ln -s /opt/FastQ-Screen/fastq_screen /usr/local/bin/fastq_screen
   fastq_screen
```

For preparing your reference for contaminations have a look how its done [here](https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/_build/html/index.html#obtaining-reference-genomes).
### **MultiQC**

```
   pip install multiqc  # Install
   multiqc .            # Run
```

### **Burrows-Wheeler Aligner**

```
  git clone https://github.com/lh3/bwa.git
	cd bwa; sudo make
  chmod 755 /opt/FastQ-Screen/fastq_screen
  sudo ln -s /opt/bwa/bwa /usr/local/bin/bwa
  cd; bwa
```
For preparing your references have a look [here](https://bio-bwa.sourceforge.net/bwa.shtml).

### **bedtools**

As an example. First, download bedtools.static.binary from the latest release [here](https://github.com/arq5x/bedtools2/releases)

```
mv bedtools.static.binary bedtools
chmod a+x bedtools
```

alternative for Linux/Ubuntu:
```
apt-get install bedtools
```
### **samtools**
```
cd samtools-1.x    # and similarly for bcftools and htslib
./configure --prefix=/where/to/install
make
make install
```
The executable programs will be installed to a bin subdirectory under your specified prefix, so you may wish to add this directory to your $PATH:
```
export PATH=/where/to/install/bin:$PATH    # for sh or bash users

setenv PATH /where/to/install/bin:$PATH    # for csh users
```
## Usage

There are two usage levels for the script:

- The first level contains a generic help message [-h], the verbose level depiction [-v] followed by a selection parameter for the subscript [binning, peak_calling].
- Each subsequent script has a specific usage message and (non-)optional arguments.

`python3 detect_EdU_peaks.py --help`

```bash
usage: detect_EdU_peaks.py [-h] [-v v] {prepare_reference,binning,peak_calling,2bw,peak_annotation} ...

Peak detection for Edu-Seq: binning, peak calling and conversion to .bw for genome browser visualisations.

positional arguments:
  {prepare_reference,binning,peak_calling,2bw,peak_annotation}

optional arguments:
  -h, --help            show this help message and exit
  -v v, --verbose v     levels: 0-none,1-info,2-debug

```

`python3 detect_EdU_peaks.py prepare_reference --help`

```bash
usage: detect_EdU_peaks.py prepare_reference [-h] -b BAM [-s BIN_SIZE] [-r READ_LENGTH] [-o READ_LENGTH_OFFSET] [-m MAPPING_QUALITY] [-d DEFAULT_UCSC] [-t THREADS]

optional arguments:
  -h, --help            show this help message and exit
  -b BAM, --bam BAM     Aligned bam file from the refence. The resulting count file will serve as the adjustment WGS sample file
  -s BIN_SIZE, --bin_size BIN_SIZE
                        The size of each bin for the genome. (default: 10000)
  -r READ_LENGTH, --read_length READ_LENGTH
                        The size of each read. (default: 100)
  -o READ_LENGTH_OFFSET, --read_length_offset READ_LENGTH_OFFSET
                        If you want to also have shorter reads as part of high quality filtering. (default: 5)
  -m MAPPING_QUALITY, --mapping_quality MAPPING_QUALITY
                        Quality threshold of mapped reads to choose for the reference adjust file. (default score from bwa-mem: 60)
  -d DEFAULT_UCSC, --default_ucsc DEFAULT_UCSC
                        Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: GRCh38
  -t THREADS, --threads THREADS
                        Number of threads to use for bam file processing: 1
```

`python3 detect_EdU_peaks.py binning --help`

```bash
usage: detect_EdU_peaks.py binning [-h] -b BAM [-s BIN_SIZE] -a ADJUST [-r READ_LENGTH] [-o READ_LENGTH_OFFSET] [-m MAPPING_QUALITY] [-d DEFAULT_UCSC] [-t THREADS]

optional arguments:
  -h, --help            show this help message and exit
  -b BAM, --bam BAM     Aligned bam file.
  -s BIN_SIZE, --bin_size BIN_SIZE
                        The size of each bin for the genome. (default: 10000)
  -a ADJUST, --adjust ADJUST
                        Countmatrix file of the adjustment WGS sample:
  -r READ_LENGTH, --read_length READ_LENGTH
                        The size of each read. (default: 100)
  -o READ_LENGTH_OFFSET, --read_length_offset READ_LENGTH_OFFSET
                        If you want to also have shorter reads as part of high quality filtering. (default: 5)
  -m MAPPING_QUALITY, --mapping_quality MAPPING_QUALITY
                        Quality threshold of mapped reads to choose for binning. (default score from bwa-mem: 60)
  -d DEFAULT_UCSC, --default_ucsc DEFAULT_UCSC
                        Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: GRCh38
  -t THREADS, --threads THREADS
                        Number of threads to use for bam file processing: 1
```
`python3 detect_EdU_peaks.py peak_calling --help`

```bash
usage: detect_EdU_peaks.py peak_calling [-h] -q QUALITY_FILE -f BINNING_FILE -b BAM [-t THREADS] [-d DEFAULT_UCSC]

optional arguments:
  -h, --help            show this help message and exit
  -q QUALITY_FILE, --quality_file QUALITY_FILE
                        Quality statistics file from binning.
  -f BINNING_FILE, --binning_file BINNING_FILE
                        Binning text-file.
  -b BAM, --bam BAM     .bam binary alignment file.
  -t THREADS, --threads THREADS
                        Number of threads to use for bam file processing: 1
  -d DEFAULT_UCSC, --default_ucsc DEFAULT_UCSC
                        Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used: GRCh38
```
`python3 detect_EdU_peaks.py 2bw --help`
```bash
usage: detect_EdU_peaks.py 2bw [-h] -f CSV_FILE [-s BIN_SIZE]

 INPUTFILE: -------_sigma_all_0b_long_sorted.csv
#
# for each genomic bin in the _sigma_all_0b.csv : 0 -> 0-9999, 10000-19999

# different output files are added now as bigwig files: [...].bw

 14 = bin                            number of sequences per bin
  5 = adjust                         sequence coverage of genomic DNA sequencing (reflecting all sequencing biases)
 11 = adjbin                         number of sequences per bin, adjusted for all sequencing biases (using $adjust)
 12 = adjbin_mb                      number of sequences per bin with background (noise) subtraction, adjusted for all sequencing biases
 
  7 = sigma_bin_totalSD              sigma value without background (noise) subtraction
  3 = sigma_bin_totalSD_sm           sigma value without background (noise) subtraction, all values smoothened
  4 = sigma_bin_totalSD_tm           sigma value without background (noise) subtraction, high values trimmed

  6 = $sigma_bin_mb_totalSD           sigma value with background (noise) subtraction
  1 = $sigma_bin_mb_totalSD_sm        sigma value with background (noise) subtraction, all values smoothened
  2 = $sigma_bin_mb_totalSD_tm        sigma value with background (noise) subtraction, high values trimmed

  8 = $sigma_bin_mb_totalSD_log2      sigma value with background (noise) subtraction, converted to log2
  9 = $sigma_bin_mb_totalSD_sm_log2   sigma value with background (noise) subtraction, all values smoothened, converted to log2
 10 = $sigma_bin_mb_totalSD_tm_log2   sigma value with background (noise) subtraction, high values trimmed, converted to log2

 13 = adjbin_SD                       standard deviation of adjbin values

                                         

optional arguments:
  -h, --help            show this help message and exit
  -f CSV_FILE, --csv_file CSV_FILE
                        The ...sigma_all_0b_long_sorted.csv file from --peak_calling option
  -s BIN_SIZE, --bin_size BIN_SIZE
                        The used binsize.
```
`python3 detect_EdU_peaks.py peak_annotation --help`
```bash
usage: detect_EdU_peaks.py peak_annotation [-h] -f CSV_FILE -r REF_FILE [-s BIN_SIZE] [-t THREADS] [-d DEFAULT_UCSC] [-p PROMINENCE_THRESHOLD] -c FRAGILE_SITES

optional arguments:
  -h, --help            show this help message and exit
  -f CSV_FILE, --csv_file CSV_FILE
                        Binning ...sigma_all_0b.csv text-file.
  -r REF_FILE, --ref_file REF_FILE
                        Reference text file ...sigma_all_0b.csv text-file.
  -s BIN_SIZE, --bin_size BIN_SIZE
                        The used binsize. default: 10000
  -t THREADS, --threads THREADS
                        Number if threads to use for bam file processing: 10
  -d DEFAULT_UCSC, --default_ucsc DEFAULT_UCSC
                        Default genome version [GRCh38,T2T-CHM13] can be set if UCSC-style contig naming is used (Annotation is based on GRCh38 for now): GRCh38
  -p PROMINENCE_THRESHOLD, --prominence_threshold PROMINENCE_THRESHOLD
                        scipy.singal.find_peaks parameter. The prominence of a peak measures how much a peak stands out from the surrounding baseline of the signal and is defined as the vertical distance between the
                        peak and its lowest contour line: default 200
  -c FRAGILE_SITES, --fragile_sites FRAGILE_SITES
                        Midas-seq-CFS-united20230710.xlsx, Fragile site excel file, part of the data folder
```

### **Auto-identification of used Genome in the .BAM file**

Identification of used genome via used chromosome names used by GENEBANK (CM000663.2 -> chr1) or Refseq (NC_000001.11 -> chr1). If UCSC-style chromosome annotation (chr1) is detected option `-d` is regarded.

#### **Example Commands**

Note: Here we use a mockup bam file located in the data folder. It only consists of reads on chr1 and is only for demonstration purposes. Here we use it for creating the reference and also for binning and peak calling. 
On a real dataset this would not make sense. This should only demonstrate how to use the different parts of the script.

`python3 detect_EdU_peaks.py prepare_reference -b 'data/SRR6365046_genebank_sorted_1.bam' -t 15`

`python3 detect_EdU_peaks.py -v 1 binning -b /media/awolff/DATA/Projects/Data_test/SRR6365046_genebank_sorted_1.bam --threads 10 > ../count_bin_matrix.csv`

`python3 ./detect_EdU_peaks.py peak_calling -q ../bam/Edu1_S2_R1_001_sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_qual_counts_0b.txt -f ../bam/Edu1_S2_R1_001_sorted.bam_bin-size_10000_quality_60_chr1-X_adjbin_0b.csv -b ../bam/Edu1_S2_R1_001_sorted.bam`

`python3 ./detect_EdU_peaks.py  2bw -f ../../../bam/Edu1_S2_R1_001_sorted_subsampled_0.001.bam_sigma_all_0b_long_sorted.csv`

`python3 scripts/eduseq/edu-seq/detect_EdU_peaks.py peak_annotation -f bam/Edu1_S2_R1_001_sorted.bam_sigma_all_0b.csv -c scripts/eduseq/edu-seq/data/Midas-seq-CFS-united_NEW_20231109.xlsx -r bam/Reference_adjust_bin-size_10000_chr1-X_adjbin_0b.csv -p 100`

## Support

For isssues or suggestions contact Dr. Alexander Wolff at alexander.wolff@med.uni-goettingen.de

## FAQ

## Roadmap

**Python-script for binning the data**
- [x] implement a sophisticated command line interface
- [x] implement a direct .bam input support
- [x] implement parallization of binning via chunking
- [x] update the genome version to hg38
- [x] add T2TCHM13 support(only for binning yet, no peak annotation yet)
- [x] implement the binning (original implementation)
- [x] implement the binning (own implementation)


**Python-script for calculation of sigma values**
- [x] implement a sophisticated command line interface
- [x] update the genome version to hg38
- [x] implement the different stages of sigma calculations

**Python-script for annotation of peaks**
- [x] peaks are detected via the method of [peak prominence](https://en.wikipedia.org/wiki/Topographic_prominence)
- [x] all in one table with annotated genes, common fragile sites
- [x] genes are annotated via the python package pyensembl
- [x] sorted via a peak ratio based on control and edu samples bin counts
- [x] implement a .bed file output for easy motif search support via [HOMER motif analysis](http://homer.ucsd.edu/homer/motif/)
- [ ] add T2TCHM13 support (not really possible yet)

**R-script for the visualisation of peaks**
- ~~[ ] implement a sophisticated command line interface~~
- [x] update the genome version to hg38
- [x] direct support of jbrowse via supplementing multiple bigwigfiles during the calculation of sigma values step
- ~~[ ] re-implement the perl-visualizations with ggplot~~
- ~~[x] (optional) Interactive Viewer of peak positions in R-shiny?~~
- [x] The [JBrowser](https://jbrowse.org/jb2/) takes bigwig files as input for the visualization of our peaks

## Authors and acknowledgment
Dr. rer. nat. Alexander Wolff alexander.wolff@med.uni-goettingen.de

## License
Copyright (c) 2023, Dr. Alexander Wolff
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the <organization>.
4. Neither the name of the <organization> nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE 
USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Project status

Ongoing
